import numpy as np
from numba import njit, jit
from astropy.cosmology import LambdaCDM
from astropy import constants as const
import astropy.units as u
import pandas as pd
from multiprocessing import Process
import itertools
import sys
import os
import time
#----------My modules for Gadget2----------------
import readsnap
import argparse
#------------ input Params ----------------------

# Remember nbox is ngrid not box size its Lbox

#---------------------------------------------------------


#class Sampling:

#------- class to compute pair correaltion
#    def __init__(self, dir_name, nfiles, npart_sim, analysis_type): 

def main(): 
    
    """
    Parameters:
    """
    Lbox= 200
    npart_sim= 400
    nfiles=4
    snapID=117
    """
    dir_name= ("/mnt/data2/nkhandai/new_runs/dmo/"\
                   "Gadget2/planck2018/%dMpc_%d/"% (Lbox, npart_sim))
   
    dir_check = str(os.path.isdir(dir_name))
    if dir_check==False:
        raise ValueError('Input directory path does not exsist')
    
    inp_file = dir_name + "snapdir/snapshot_%0.3d.%d"%(snapID, 0)
    header = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
    
    print "============Gadget header================\n"
    print header.read_gadget_header()
    print "============Gadget header================\n"
    
    ParticleMass = header.massarr[1]
    cosmo  = LambdaCDM(H0= 100.*header.HubbleParam, 
                           Om0 = header.Omega0,
                           Ode0= header.OmegaLambda,
                           Tcmb0=0.)
    nbox    = header.BoxSize/1000.
    boxhalf = nbox/2


    #------------------------------------------------
    Npart_files = np.zeros(nfiles, dtype=np.uint64) 
    nfile_index = np.arange(nfiles, dtype=np.int32)
    
    for i in xrange(nfiles):
        inp_file = dir_name + "snapdir/snapshot_%03d.%d"%(snapID, i)
        file_check = str(os.path.isfile(inp_file))
        if file_check==False:
            raise ValueError('Input file does not exsist')
        snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
        Npart_files[i] = snapshot.npart[1]
        print inp_file, "npart:",snapshot.npart[1]
                   
    """               
    
    print ""
    print("Enter the size of subsample you want to generate index for\n")
    print ""
    nsub=np.uint32(raw_input(""))

    np.random.seed(123456)
    nfile_index= np.arange(6)
    Npart_files = np.array([10, 90, 100, 20, 80, 200]) 
    print"\n"
    #index = np.random.randint(snapshot.npartTotal[1], size=nsub)
    index = np.random.randint(500, size=nsub)
    print index
    print"\n"
    print Npart_files
    print"\n"
    npart_cumsum = np.cumsum(Npart_files)
    npart_cumsum1 = npart_cumsum-Npart_files
    print npart_cumsum
    print"\n"
    print npart_cumsum1
    print"=========================================================\n"
    file_index = np.arange(int(nfiles))
    count=0
    for i in index:
        if i!=0:
            tmp = (i<=npart_cumsum)
            tmp1 = npart_cumsum[tmp]
            
            print  i, (tmp1)[0], (Npart_files[tmp])[0], npart_cumsum1[tmp][0], (nfile_index[tmp])[0], np.uint64(abs(npart_cumsum1[tmp][0] - i))
            print "\n"
            print "index:", i, "Npart_file:", (Npart_files[tmp])[0],"ncumsum:", npart_cumsum1[tmp][0], "nfile:", (nfile_index[tmp])[0], "index:", np.uint64(abs(npart_cumsum1[tmp][0] - i))
            print "\n"
            count+=1 

if __name__ == "__main__":
    main()


            

#---------------------------------------------------------------------

