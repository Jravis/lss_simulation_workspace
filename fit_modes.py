import numpy as np
import matplotlib.pyplot as plt
import lmfit
import emcee
import corner
import Cosmo_Growth_Fac_raw as CGF_raw
import sys

import matplotlib as mpl
import readsnap

#mpl.rcParams['font.family'] = 'sans-serif'
#mpl.rcParams['font.sans-serif'] = ['Tahoma']
#mpl.rcParams['text.usetex'] = True
#plt.style.use("classic")



#------------ input Params ----------------------

# Remember nbox is ngrid not box size its Lbox

Lbox = np.int32(sys.argv[1])
nbox = np.int32(sys.argv[2])
snapnumber = np.int32(sys.argv[3])
npart_sim = np.int32(sys.argv[4])


#dir_name= ("/mnt/data2/nkhandai/new_runs/dmo/"\
#            "Gadget2/planck2018/%dMpc_%d/"% (Lbox, npart_sim))
#inp_file = dir_name+'snapdir/snapshot_%0.3d.%d'%(snapnumber, 0)

dir_name = "/mnt/data1/sandeep/New_Gadget2_run/%dMpc_%d/"%(Lbox, npart_sim)
inp_file = dir_name + "snapdir_seed_1690811/snapshot_005.0"
print inp_file 
snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
print snapshot.read_gadget_header()

OmegaMatter = snapshot.Omega0
OmegaLambda = snapshot.OmegaLambda
H0 = snapshot.HubbleParam*100.

model = CGF_raw.Growth_Factor(h=snapshot.HubbleParam, Om=OmegaMatter, 
                               Onu=OmegaLambda, amin=1e-5, dlna=0.0001)
bias = 1
D = model.growth_factor_fnc(snapshot.redshift)    
beta = model.growth_rate_fnc(snapshot.redshift)/bias
print "================================"
print""
print "Beta value:%f"%beta
print ""
print "================================"


#dirc_name = "/mnt/data1/sandeep/Simulation_data/Cross_Power_Mode_Count_Data/"

dir_name = ("/mnt/data1/sandeep/New_Gadget2_run/"\
            "post_simulation_analysis/%dMpc_%d_analysis/"% (Lbox, npart_sim))

fname = dir_name + "sorted_modes_1000_1024_005_9.txt"
Delta_m_r = np.genfromtxt(fname, usecols=5)
Delta_m_c = np.genfromtxt(fname, usecols=6)
Delta_f_r = np.genfromtxt(fname, usecols=7)
Delta_f_c = np.genfromtxt(fname, usecols=8)
mu_sqr= np.genfromtxt(fname, usecols=3)
pars = lmfit.Parameters()
pars.add('beta', value=2.0, min=0.0, max=10.0)
pars.add('bias', value=1.0, min=0.0, max=4.0)
pars.add('lnPn', value = 5, min=-10,max= 10)


def residual(p1):    
    
    beta = p1['beta'].value
    bias = p1['bias'].value
    lnPn = p1['lnPn'].value
    
    tmp = bias*(1+beta*mu_sqr)
    tmp = Delta_f_r-(Delta_m_r*tmp)
    tmp1 = bias*(1. + beta*mu_sqr)
    tmp1 = Delta_f_c-(Delta_m_c*tmp1)

    return  (tmp*tmp)/np.exp(lnPn) + len(Delta_m_r)*lnPn + (tmp1*tmp1)/np.exp(lnPn) + len(Delta_m_r) * lnPn 

def lnprob(p1):
    resid = residual(p1)
    return -0.5 * np.sum(resid)


mi = lmfit.Minimizer(residual, pars)
out1 = mi.minimize(method='Nelder')

#print out1.params

mini = lmfit.Minimizer(lnprob, out1.params)
#mini = lmfit.Minimizer(lnprob, pars)
res = mini.emcee(burn=300, steps=2000, thin=10, nwalkers= 100,  is_weighted=False, 
                    params=mi.params, seed=123465)



print('------------------------------------------')
print("median of posterior probability distribution")
print('------------------------------------------')
lmfit.report_fit(res.params)


fname = dir_name+"sorted_modes_100_1024_005_9_bestfit.txt"
np.savetxt(fname,res.flatchain)
fig1 = corner.corner(res.flatchain, bins=100, labels=res.var_names,
                     truths=list(res.params.valuesdict().values()),
                     title_fmt=".2f", quantiles=[0.16, 0.84],
                     levels= [1-np.exp(-0.5),2*(1-np.exp(-0.5))],
                     show_titles=True, labels_args={"fontsize": 40},
                     fill_contours=False,hist_kwargs={'color':'k'},
                     contourf_kwargs={'linewidths': 3,'colors':('darkturquoise')} )
                     

fig1.set_size_inches(8, 6.5)
fname = dir_name+"sorted_modes_1000_1024_005_9_bestfit.png"
fig1.savefig(fname, dpi=300)



highest_prob = np.argmax(res.lnprob)
hp_loc = np.unravel_index(highest_prob, res.lnprob.shape)
mle_soln = res.chain[hp_loc]
for i, par in enumerate(pars):
    pars[par].value = mle_soln[i]

print("\nMaximum likelihood Estimation")
print('-----------------------------')
print(pars)



print("\nFinally lets work out 2-sigma error estimate")

quantiles = np.percentile(res.flatchain['beta'], [2.28, 15.9, 50, 84.2, 97.7])
print("2 sigma spread in a", 0.5 * (quantiles[-1] - quantiles[0]))
quantiles = np.percentile(res.flatchain['bias'], [2.28, 15.9, 50, 84.2, 97.7])
print("2 sigma spread in b", 0.5 * (quantiles[-1] - quantiles[0]))
quantiles = np.percentile(res.flatchain['lnPn'], [2.28, 15.9, 50, 84.2, 97.7])
print("2 sigma spread in c", 0.5 * (quantiles[-1] - quantiles[0]))



#plt.figure(2, figsize=(8, 6))
#plt.plot(res.flatchain['lnPn'], 'k-', alpha=0.3)
#plt.savefig('/dataspace/sandeep/Nishi_plots/for_sandeep_saili/lnPn_chain.pdf', dpi=300)

#plt.figure(3, figsize=(8, 6))
#plt.plot(res.flatchain['bias'], 'r-', alpha=0.3)
#plt.savefig('/dataspace/sandeep/Nishi_plots/for_sandeep_saili/bias_chain.pdf', dpi=300)

#plt.figure(4, figsize=(8, 6))
#plt.plot(res.flatchain['beta'], 'b-', alpha=0.3)
#plt.savefig('/dataspace/sandeep/Nishi_plots/for_sandeep_saili/beta_chain.pdf', dpi=300)

plt.show()
