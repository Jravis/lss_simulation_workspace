"""
This is genralised plotting class 
for simulation out plots will add
more plotting routine with time.
@author Sandeep Rana
"""


import sys
import os
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import rcParams
import matplotlib as mpl
from matplotlib.colors import LogNorm
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.colors import ListedColormap


rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Tahoma']
mpl.rcParams['text.usetex'] = True
plt.style.use("classic")

#==============Plotting slice==============
#cmap            = 'cividis'
#cmap            = 'Greys'
#cmap            = 'Blues'
#custom2 = custom_div_cmap(100, mincol='lightskyblue', midcol='deepskyblue' ,maxcol='red')


class SimPlots:

#------- class to compute growth factor

    def __init__(self, header, nbox, snapnumber):
        
        """
        Parameters:
        header: Gadget2 header
        Position: Position data frame
        nbox is 
        """
        
        self.header = header
        self.nbox =  nbox
        self.snapNo = snapnumber

    def custom_div_cmap(numcolors=50, name='custom_div_cmap',
                    mincol='blue', midcol='blue', maxcol='red'):
        """ 
        Create a custom diverging colormap with three colors
        Default is blue to white to red with 11 colors.  Colors can be specified
        in any way understandable by matplotlib.colors.ColorConverter.to_rgb()
        """
       cmap_out = LinearSegmentedColormap.from_list(name=name, 
                                                colors =[mincol, midcol, 
                                                maxcol], N=numcolors)
       return cmap_out

    def plot_density(self, rhodm, dirname, cmap_in, RSD=False, iaxis=0, custom=False):
        """
        Generlised plotting class will add more function with time.
        """
        
        print np.amax(rhodm)
        print np.amin(rhodm)
        ind = (rhodm==np.amin(rhodm))
        value = np.amin(rhodm[rhodm!=np.amin(rhodm)])
        rhodm[ind] = value
        print np.amax(rhodm)
        print np.amin(rhodm)
        if not RSD:
            fout ='delta_PlusOne_snapshot_%d_%0.1fMpc_%d_Image_%s_Real.png'%(self.snapNo,self.header.BoxSize/1000.,nbox)
            title = 'Real Space'
        else:
            fout ='delta_PlusOne_snapshot_%d_%0.1fMpc_%d_Image_%s_RSD.png'%(self.snapNo,self.header.BoxSize/1000.,nbox)
            title = 'RSD Space'
            
        if not custom:
            cmap=cmap_in
        else:
            cmap = custom_div_cmap(50, mincol='deepskyblue', midcol='deepskyblue' ,maxcol='red')
            
        fout = dir_name+fout
        fig = plt.figure(1, figsize=(12, 8))
        gs = gridspec.GridSpec(1, 1)
        ax1 = plt.subplot(gs[0, 0]) 

        im = ax1.imshow(rhodm, origin= 'lower', cmap=cmap, 
                    extent=[0, np.int32(self.header.BoxSize/1000.), 0, 
                    np.int32(self.header.BoxSize/1000.)], interpolation='none', 
                    norm = LogNorm(vmin=np.amin(rhodm), vmax=np.amax(rhodm))
        
        if iaxis==0:
            ax1.set_ylabel(r"$\mathrm{z}(\rm{kpc}$ ${h}^{-1})$", fontsize=20)
            ax1.set_xlabel(r"$\mathrm{x}(\rm{kpc}$ ${h}^{-1})$", fontsize=20)
        if iaxis==1:
            ax1.set_ylabel(r"$\mathrm{y}(\rm{kpc}$ ${h}^{-1})$", fontsize=20)
            ax1.set_xlabel(r"$\mathrm{x}(\rm{kpc}$ ${h}^{-1})$", fontsize=20)
        if iaxis==2:
            ax1.set_ylabel(r"$\mathrm{z}(\rm{kpc}$ ${h}^{-1})$", fontsize=20)
            ax1.set_xlabel(r"$\mathrm{y}(\rm{kpc}$ ${h}^{-1})$", fontsize=20)

        ax1.set_title(title)
        ax1.minorticks_on()
        ax1.legend(loc=4, frameon=False, ncol=1, fontsize=14)
        ax1.tick_params(axis='both', which='minor', length=2,
                              width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=5,
                               width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.8)
        ax1.spines['left'].set_linewidth(1.8)
        ax1.spines['top'].set_linewidth(1.8)
        ax1.spines['right'].set_linewidth(1.8)
        plt.tight_layout()
        cbar = plt.colorbar(im)
        cbar.set_label(r"$\rho/\bar{\rho}$",fontsize=20)
        plt.savefig(fout, bbox_inches='tight')
        fig.close()

#-------------------------------------------------------------------------

    def plot_Pos(self, Pos, dirname, cmap_in, RSD=False, iaxis=0, custom=False):
        
        if not RSD:
            fout ='ScatterPlt_snapshot_%d_%0.1fMpc_%d_Image_%s_Real.png'%(self.snapNo,self.header.BoxSize/1000.,nbox)
            title = 'Real Space'
        else:
            fout ='ScatterPlt_snapshot_%d_%0.1fMpc_%d_Image_%s_RSD.png'%(self.snapNo,self.header.BoxSize/1000.,nbox)
            title = 'RSD Space'
            
        if not custom:
            cmap=cmap_in
        else:
            cmap = custom_div_cmap(50, mincol='deepskyblue', midcol='deepskyblue' ,maxcol='red')
            
        fout = dir_name+fout
        fig = plt.figure(1, figsize=(12, 8))
        gs = gridspec.GridSpec(1, 1)
        ax1 = plt.subplot(gs[0, 0]) 

        if iaxis==0:
            if not RSD:
                ax1.scatter(Pos.x.values, Pos.z.values,s=1, lw=0, color='crimson', label='')
            else:
                ax1.scatter(Pos.x.values, Pos.sz.values,s=1, lw=0, color='crimson', label='')
            ax1.set_ylabel(r"$\mathrm{z}(\rm{kpc}$ ${h}^{-1})$", fontsize=20)
            ax1.set_xlabel(r"$\mathrm{x}(\rm{kpc}$ ${h}^{-1})$", fontsize=20)
        if iaxis==1:
            if not RSD:
                ax1.scatter(Pos.x.values, Pos.sy.values,s=1, lw=0, color='crimson', label='')
            else:
                ax1.scatter(Pos.x.values, Pos.sy.values,s=1, lw=0, color='crimson', label='')
            ax1.set_ylabel(r"$\mathrm{y}(\rm{kpc}$ ${h}^{-1})$", fontsize=20)
            ax1.set_xlabel(r"$\mathrm{x}(\rm{kpc}$ ${h}^{-1})$", fontsize=20)
        if iaxis==2:
            if not RSD:
                ax1.scatter(Pos.y.values, Pos.sz.values,s=1, lw=0, color='crimson', label='')
            else:
                ax1.scatter(Pos.y.values, Pos.sz.values,s=1, lw=0, color='crimson', label='')
            ax1.set_ylabel(r"$\mathrm{z}(\rm{kpc}$ ${h}^{-1})$", fontsize=20)
            ax1.set_xlabel(r"$\mathrm{y}(\rm{kpc}$ ${h}^{-1})$", fontsize=20)
        ax1.set_title(title)

        ax1.minorticks_on()
        ax1.legend(loc=4, frameon=False, ncol=1, fontsize=14)
        ax1.tick_params(axis='both', which='minor', length=5,
                              width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
                               width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.8)
        ax1.spines['left'].set_linewidth(1.8)
        ax1.spines['top'].set_linewidth(1.8)
        ax1.spines['right'].set_linewidth(1.8)
        plt.tight_layout()
        cbar = plt.colorbar(im)
        cbar.set_label(r"$\rho/\bar{\rho}$",fontsize=20)
        plt.savefig(fout, bbox_inches='tight')
        fig.close()
        



#-------------------------------------------------------------------------

    def single_plot_lp(self, x, y, dirname, xlim, ylim, 
                  clr, mt, ms, mfc, ls, lw, leg_loc, 
                  leg_font, lbl, log=0, plt_name, extension, dpi):
        
            
        fout =plt_name+extension
        fout = dir_name+fout
        gs = gridspec.GridSpec(1, 1)
        ax1 = plt.subplot(gs[0, 0]) 
        ax1.plot(M, norm,linestyle=ls, marker=mt, color=clr, 
                ms=ms, mfc=mfc, mew=1.5, mec=clr, 
                linewidth=lw, label=lbl)
        if log==1
            ax1.xscale("log")
            ax1.yscale("log")
        if log==2
            ax1.xscale("log")
        if log==3
            ax1.yscale("log")
            
        ax1.set_xlim(xlim[0], xlim[1])
        ax1.set_ylim(ylim[0], ylim[1])
        ax1.minorticks_on()
        ax1.legend(loc=leg_loc, frameon=False, ncol=1, fontsize=leg_font)
        ax1.tick_params(axis='both', which='minor', length=5,
                              width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
                               width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.8)
        ax1.spines['left'].set_linewidth(1.8)
        ax1.spines['top'].set_linewidth(1.8)
        ax1.spines['right'].set_linewidth(1.8)
        fig.savefig(fout, bbox_inches='tight', dpi=dpi)
        










