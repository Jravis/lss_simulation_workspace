import numpy as np
import os
import sys
import Gen_Power_Sim
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from matplotlib import rcParams
import matplotlib as mpl 
import time
from matplotlib.colors import LogNorm
from matplotlib.colors import LinearSegmentedColormap
#sys.path.append('/mnt/home1/sandeep/workspace/
#Simulations_workspace/python_routines/Gadget2_routines/')
import readsnap
import smoothden
#import _SmoothDen
#import smoothdensity as sm

rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Tahoma']
mpl.rcParams['text.usetex'] = True
plt.style.use("classic")
from matplotlib.colors import ListedColormap

Splotch_colorscale = ['Blue',  'Fancy',  'Klaus',  'LongFancy', 
                      'M51',   'M51_stars',  'Martin', 
                      'NewSplotch',  'OldSplotch', 'OrionNew1',  
                      'Orion', 'RedBlue', 'Stars', 'Tipsy', 
                      'Yellow']
scale_clr = [3, 7, 5, 15, 255, 5, 6, 6, 3, 7, 7, 3, 3, 5, 4]
def _colormaps(name, scl):
    dirc = "palettes/"+name+'.pal'
    colombi1_cmap = ListedColormap(np.loadtxt(dirc)/scl)


#------------ input Params ----------------------

# Remember nbox is ngrid not box size its Lbox

Lbox = np.int32(sys.argv[1])
nbox = np.float32(sys.argv[2])
snapnumber = np.int32(sys.argv[3])
nfiles = np.int32(sys.argv[4])
npart_sim = np.int32(sys.argv[5])

bias = [10, 100, 500]

# Class initialization
Sim_pow = Gen_Power_Sim.Sim_Power(np.int32(nbox), Lbox, snapnumber, npart_sim)
Sim_pow.setk()
Sim_pow.setweights()
Sim_pow.setdeconv()


#snapshots = np.array([117, 108, 98, 94, 89, 86, 85, 67,
#                       58, 50, 42, 36, 31, 27, 23, 19])  
#------------------------------------------------
#dir_name= "/mnt/data1/sandeep/New_Gadget2_run/snapshot_rhodm"



#dir_name= "/mnt/data1/sandeep/Simulation_data/rhodm_data"
dir_name = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/%dMpc_%d_analysis/"% (Lbox, npart_sim)

#real_field = dir_name+"/%dMpc_400_REAL/rhodm_%dMpc_%d_%d.npy"%(Lbox, Lbox, nbox, snapnumber)

real_field = dir_name+"rhodm_snapshot_1690811/rhodm_%dMpc_%d_%d.npy"%(Lbox, np.int32(nbox), snapnumber)
rhodm_real = np.load(real_field)

"""
for bb in bias:
    rhodm_bias_field = bb * np.load(real_field)
    #rhodm_bias_field =  np.load(real_field)
    #ind = (rhodm_bias_field < bb)
    #rhodm_bias_field[ind] = 0.
    #print "------------------------"
    #print rhodm_bias_field[~ind]
    #print rhodm_bias_field[ind]
    #print "-------------------------"
    rhodm_real = np.load(real_field)
    Sim_pow.compute_3d_CrossPower(rhodm_bias_field, rhodm_real, 24, bb, True)
    del rhodm_bias_field
    del rhodm_real

file_out = "/mnt/data1/sandeep/Simulation_data/Cross_Power_Mode_Count_Data/"
file_out = file_out+"3dCrossPow_dm_%dMpc_%d_%d_thresh_%d_scaled_test.txt"%(Lbox, nbox, snapnumber, bias[0])
#file_out = file_out+"3dCrossPow_dm_%dMpc_%d_%d_thresh_%d_scaled.txt"%(Lbox, nbox, snapnumber, bias[0])

K = np.genfromtxt(file_out, usecols=0)


crosscoeff_real = np.zeros((len(bias), len(K)), dtype=np.float64)
bias_real = np.zeros((len(bias), len(K)), dtype=np.float64)
cross_pow_real = np.zeros((len(bias), len(K)), dtype=np.float64)
cross_pow_imag = np.zeros((len(bias), len(K)), dtype=np.float64)

pow_field = np.zeros((len(bias), len(K)), dtype=np.float64)
pow_matter = np.zeros((len(bias), len(K)), dtype=np.float64)
count = 0
for bb in bias:
    file_out = "/mnt/data1/sandeep/Simulation_data/Cross_Power_Mode_Count_Data/"
    file_out = file_out+"3dCrossPow_dm_%dMpc_%d_%d_thresh_%d_scaled_test.txt"%(Lbox, nbox, snapnumber, bb)
    #file_out = file_out+"3dCrossPow_dm_%dMpc_%d_%d_thresh_%d_scaled.txt"%(Lbox, nbox, snapnumber, bb)
    
    pow_field[count,:] = np.genfromtxt(file_out, usecols=1)
    pow_matter[count,:] = np.genfromtxt(file_out, usecols=2)
    
    cross_pow_real[count,:] = np.genfromtxt(file_out, usecols=3)
    cross_pow_imag[count,:] = np.genfromtxt(file_out, usecols=4)
    crosscoeff_real[count,:] = np.genfromtxt(file_out, usecols=5)
    bias_real[count,:] = np.genfromtxt(file_out, usecols=7)
    count+=1


file_out = "/mnt/data1/sandeep/Simulation_data/Cross_Power_Mode_Count_Data/"

fig= plt.figure(1, figsize=(8, 6))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])

ax1.plot((K), (cross_pow_real[0,:]), linestyle='-', color='r',
         linewidth=2.5, label=r"$\delta_f = %d \delta_m$"%(bias[0]))
ax1.plot((K), (cross_pow_real[1,:]), linestyle='-', color='b',
         linewidth=2.5, label=r"$\delta_m = %d \delta_m$"%(bias[1]))
ax1.plot((K), (cross_pow_real[2,:]), linestyle='-', color='g',
         linewidth=2.5, label=r"$\delta_m = %d \delta_m$"%(bias[2]))

ax1.set_title(r"$\rm{Ngrid}:1024, Z = 0.0$", fontsize=16)
ax1.set_xlabel(r'$\rm{k}[\rm{h}/\rm{Mpc}]$', fontsize=20)
ax1.set_ylabel(r'$\Delta^2_{\delta_f \delta_m}$', fontsize=20)
#ax1.set_ylim(0.001, 1000)
#ax1.set_xlim(0.01, 100)
ax1.set_xscale("log")
ax1.set_yscale("log")
ax1.minorticks_on()
ax1.legend(frameon=False, ncol=1, fontsize=16, loc=4)
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
                       
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
fout = file_out + "CrossPow_diffbias_Lbox_%d_nbox_%d_snapshot_%d_Npart_%d_real_test.png"%(Lbox, nbox, snapnumber, npart_sim)
#fout = file_out + "CrossPow_diffbias_Lbox_%d_nbox_%d_snapshot_%d_Npart_%d_real.png"%(Lbox, nbox, snapnumber, npart_sim)
fig.savefig(fout, bbox_inches='tight')



#--------------------------------------------------------------------------------------


fig= plt.figure(2, figsize=(8, 6))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])

ax1.plot((K), (cross_pow_imag[0,:]), linestyle='-', color='r',
         linewidth=2.5, label=r"$\delta_f = %d\delta_m$"%(bias[0]))

ax1.plot((K), (cross_pow_imag[1,:]), linestyle='--', color='b',
         linewidth=2.5, label=r"$\delta_m = %d\delta_m$"%(bias[1]))

ax1.plot((K), (cross_pow_imag[2,:]), linestyle='-.', color='g',
         linewidth=2.5, label=r"$\delta_m = %d\delta_m$"%(bias[2]))
         
ax1.set_title(r"$\rm{Ngrid}:1024, Z = 0.0$", fontsize=16)
ax1.set_xlabel(r'$\rm{k}[\rm{h}/\rm{Mpc}]$', fontsize=20)
ax1.set_ylabel(r'$\Delta^2_{\delta_f \delta_m}$', fontsize=20)
#ax1.set_ylim(0.001, 1000)
#ax1.set_xlim(0.01, 100)

ax1.set_xscale("log")
#ax1.set_yscale("log")
ax1.minorticks_on()
ax1.legend(frameon=False, ncol=1, fontsize=16, loc=3)
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
fout = file_out + "CrossPow_diffbias_Lbox_%d_nbox_%d_snapshot_%d_Npart_%d_imag_test.png"%(Lbox, nbox, snapnumber,  npart_sim)
#fout = file_out + "CrossPow_diffbias_Lbox_%d_nbox_%d_snapshot_%d_Npart_%d_imag.png"%(Lbox, nbox, snapnumber,  npart_sim)
fig.savefig(fout, bbox_inches='tight')


#----------------------------------------------------------------------------------------------

fig= plt.figure(3, figsize=(8, 6))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])

ax1.plot((K), (pow_field[0,:]), linestyle='-', color='r',
         linewidth=2.5, label=r"$\delta_f = %d\delta_m$"%(bias[0]))
ax1.plot((K), (pow_field[1,:]), linestyle='-', color='b',
         linewidth=2.5, label=r"$\delta_f = %d\delta_m$"%(bias[1]))

ax1.plot((K), (pow_field[2,:]), linestyle='-', color='g',
         linewidth=2.5, label=r"$\delta_f =  %d\delta_m$"%(bias[2]))

ax1.plot((K), (pow_matter[0,:]), linestyle='-', color='m',
         linewidth=2.5,label=r"$\delta_m$" )

ax1.set_title(r"$\rm{Ngrid}:1024, Z = 0.0$", fontsize=16)
ax1.set_xlabel(r'$\rm{k}[\rm{h}/\rm{Mpc}]$', fontsize=20)
ax1.set_ylabel(r'$\Delta^2$', fontsize=20)
#ax1.set_ylim(0.001, 1000)
#ax1.set_xlim(0.01, 100)

ax1.set_xscale("log")
ax1.set_yscale("log")
ax1.minorticks_on()
ax1.legend(frameon=False, ncol=1, fontsize=16, loc=4 )
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
fout = file_out + "autoPow_diffbias_Lbox_%d_nbox_%d_snapshot_%d_Npart_%d_imag_test.png"%(Lbox, nbox, snapnumber,  npart_sim)
#fout = file_out + "autoPow_diffbias_Lbox_%d_nbox_%d_snapshot_%d_Npart_%d_imag.png"%(Lbox, nbox, snapnumber,  npart_sim)
fig.savefig(fout, bbox_inches='tight')



#--------------------------------------------------------------------------------------


fig= plt.figure(4, figsize=(8, 6))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])

ax1.plot((K), (crosscoeff_real[0,:]), linestyle='-', color='r',
         linewidth=2.5, label=r"$\delta_f = %d\delta_m$"%(bias[0]))
ax1.plot((K), (crosscoeff_real[1,:]), linestyle='-', color='b',
         linewidth=2.5, label=r"$\delta_f = %d\delta_m$"%(bias[1]))
ax1.plot((K), (crosscoeff_real[2,:]), linestyle='-', color='g',
         linewidth=2.5, label=r"$\delta_f = %d\delta_m$"%(bias[2]))
         
ax1.set_title(r"$\rm{Ngrid}:1024, Z = 0.0$", fontsize=16)
ax1.set_xlabel(r'$\rm{log}_{10}(\rm{k})[\rm{h}/\rm{Mpc}]$', fontsize=20)
ax1.set_ylabel(r'$\Delta^2_{\delta_f \delta_m}/(\sqrt{\Delta^2_f \Delta^2_m})$', fontsize=20)
#ax1.set_ylim(0.9, 1.01)
#ax1.set_xlim(0.01, 100)
ax1.set_xscale("log")
ax1.minorticks_on()
ax1.legend(frameon=False, ncol=1, fontsize=16, loc=3 )
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
fout = file_out + "CrossCoeff_Lbox_%d_nbox_%d_snapshot_%d_Npart_%d_imag_test.png"%(Lbox, nbox, snapnumber,  npart_sim)
#fout = file_out + "CrossCoeff_Lbox_%d_nbox_%d_snapshot_%d_Npart_%d_imag.png"%(Lbox, nbox, snapnumber,  npart_sim)
fig.savefig(fout, bbox_inches='tight')


#--------------------------------------------------------------------------------------


fig= plt.figure(5, figsize=(8, 6))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])

ax1.plot(np.log10(K), (bias_real[0,:]), linestyle='-', color='r',
         linewidth=2.5, label=r"$\delta_f = %d\delta_m$"%(bias[0]))
        
ax1.plot(np.log10(K), (bias_real[1,:]), linestyle='-', color='b',
         linewidth=2.5, label=r"$\delta_f = %d\delta_m$"%(bias[1]))
         
ax1.plot(np.log10(K), (bias_real[2,:]), linestyle='-', color='g',
         linewidth=2.5, label=r"$\delta_f = %d\delta_m$"%(bias[2]))

ax1.set_title(r"$\rm{Ngrid}:1024, Z = 0.0$", fontsize=16)
ax1.set_xlabel(r'$\rm{log}_{10}(\rm{k})[\rm{h}/\rm{Mpc}]$', fontsize=20)
ax1.set_ylabel(r'$\rm{b}$', fontsize=20)
ax1.set_ylim(1, 550)
#ax1.set_xlim(-2, 2)
ax1.minorticks_on()
ax1.legend(frameon=False, ncol=1, fontsize=16, loc=1)
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
fout = file_out + "bias_Lbox_%d_nbox_%d_snapshot_%d_Npart_%d_imag_test.png"%(Lbox, nbox, snapnumber,  npart_sim)
#fout = file_out + "bias_Lbox_%d_nbox_%d_snapshot_%d_Npart_%d_imag_.png"%(Lbox, nbox, snapnumber,  npart_sim)
fig.savefig(fout, bbox_inches='tight')

plt.show()

"""


#rsd_field = dir_name+"/%dMpc_400_RSD/test_rhodm_%dMpc_%d_%d.npy"%(Lbox, Lbox, nbox, snapnumber)
rsd_field = dir_name+"rhodm_snapshot_1690811/rhodm_%dMpc_%d_%d_RSD.npy"%(Lbox, np.int32(nbox), snapnumber)
rhodm_rsd = np.load(rsd_field)
print "Input number of maximum modes"
Nmax= np.float64(raw_input(""))
Sim_pow.compute_modes(rhodm_real, rhodm_rsd, 30, Nmax)









