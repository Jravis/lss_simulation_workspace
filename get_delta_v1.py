import numpy as np
import readsnap
import os
import sys
import pandas as pd
import smoothden
import Gen_Power_Sim
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib as mpl 
import time
from matplotlib.colors import LogNorm
from matplotlib.colors import ListedColormap
from matplotlib.colors import LinearSegmentedColormap

mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['font.sans-serif'] = ['Tahoma']
mpl.rcParams['text.usetex'] = True
plt.style.use("classic")


#import _SmoothDen
#import smoothdensity as sm

#------------ input Params ----------------------

# Remember nbox is ngrid not box size its Lbox

Lbox = np.int32(sys.argv[1])
nbox = np.float32(sys.argv[2])
snapnumber1 = np.int32(sys.argv[3])
nfiles = np.int32(sys.argv[4])
npart_sim = np.int32(sys.argv[5])

#------------------------------------------------
#dir_name= ("/mnt/data2/nkhandai/new_runs/dmo/"\
#            "Gadget2/planck2018/%dMpc_400/"% (Lbox))

dir_name = "/mnt/data1/sandeep/New_Gadget2_run/%dMpc_%d/"% (Lbox, npart_sim)
#snapshots=[2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
snapshots=[0, 1, 2, 3, 4, 5, 6]
for snapnumber in snapshots:
    for i in xrange(nfiles):

        inp_file = dir_name + "snapdir_seed_1690811/snapshot_%0.3d.%d"%(snapnumber, i)
        print inp_file 
        snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
#    print "============Gadget header================"
        #print snapshot.read_gadget_header()
        #snapshot_header =  snapshot.read_gadget_header()

        if i==0:
            Positions_df  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
            Positions_df.columns=['x','y','z']
            Velocity_df   = snapshot.read_Vel()
            Velocity_df.columns=['vx','vy','vz']

        if i==1:
            Positions_df1  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
            Positions_df1.columns=['x','y','z']
            Velocity_df1   = snapshot.read_Vel()
            Velocity_df1.columns=['vx','vy','vz']

        if i==2:
            Positions_df2  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
            Positions_df2.columns=['x','y','z']
            Velocity_df2   = snapshot.read_Vel()
            Velocity_df2.columns=['vx','vy','vz']

        if i==3:
            Positions_df3  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
            Positions_df3.columns=['x','y','z']
            Velocity_df3   = snapshot.read_Vel()
            Velocity_df3.columns=['vx','vy','vz']


# -------------------------Merging all data frames---------------

    tmp = Positions_df.append(Positions_df1, ignore_index=True, sort=False)
    tmp1 = tmp.append(Positions_df2, ignore_index=True, sort=False)
    tmp2 = tmp1.append(Positions_df3, ignore_index=True, sort=False)
    Positions_df = tmp2

    del Positions_df1
    del Positions_df2 
    del Positions_df3

# ===Merging all Vel data frames============================

    tmp = Velocity_df.append(Velocity_df1, ignore_index=True, sort=False)
    tmp1 = tmp.append(Velocity_df2, ignore_index=True, sort=False)
    tmp2 = tmp1.append(Velocity_df3, ignore_index=True, sort=False)
    Velocity_df = tmp2

    del Velocity_df1
    del Velocity_df2
    del Velocity_df3

    print snapshot.HubbleParam

    """
    print "Following is the key for what you want with the data\n"
    print "Key = 0 no RSD and 3D CIC"
    print "Key = 1 yes RSD and 3D CIC"
    print "Key = 2 no RSD and 2D CIC"
    print "Key = 3 yes RSD and 2D CIC"

    print ""
    key = np.int32(raw_input(""))
    print "Key:%d"% key
    """
    
    print ""
    print '-----------------------------------------------------'
#------------------------------------------------------------------

    dir_name1 = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/%dMpc_%d_analysis/"% (Lbox, npart_sim)
    CIC_object = smoothden.SmoothDensity(snapshot, np.int32(nbox))
    rhodm, rhodm_avg = CIC_object.SmoothDen3d(Positions_df, Velocity_df, True)
    print rhodm_avg
    print rhodm.sum()/np.float64(nbox)**3.
    rhodm = rhodm/rhodm_avg 
    print np.amax(rhodm)
    print np.amin(rhodm)
    outfile=dir_name1+"rhodm_snapshot_1690811/rhodm_%dMpc_%d_%d_RSD.npy"%(Lbox, np.int32(nbox), snapnumber)
    np.save(outfile, rhodm)
    Sim_pow = Gen_Power_Sim.Sim_Power(np.int32(nbox), Lbox, snapnumber, npart_sim)
    Sim_pow.setk()
    Sim_pow.setweights()
    Sim_pow.setdeconv()
    Sim_pow.compute_3dpower(rhodm, 28, True)




"""
if key==0:
    CIC_object = smoothden.SmoothDensity(snapshot, np.int32(nbox))
    rhodm, rhodm_avg = CIC_object.SmoothDen3d(Positions_df, Velocity_df)
    print rhodm_avg
    print rhodm.sum()/np.float64(nbox)**3.
    rhodm = rhodm/rhodm_avg 
    print np.amax(rhodm)
    print np.amin(rhodm)
    outfile=dir_name+"rhodm_snapshot/%dMpc_%d_REAL/rhodm_%dMpc_%d_%d.npy"%(Lbox, npart_sim, Lbox, np.int32(nbox), snapnumber)
    np.save(outfile, rhodm)
    Sim_pow = Gen_Power_Sim.Sim_Power(np.int32(nbox), Lbox, snapnumber, npart_sim)
    Sim_pow.setk()
    Sim_pow.setweights()
    Sim_pow.setdeconv()
    Sim_pow.compute_3dpower(rhodm, 24, True)


if key==1:
    CIC_object = smoothden.SmoothDensity(snapshot, np.int32(nbox))
    rhodm_RSD, rhodm_avg = CIC_object.SmoothDen3d(Positions_df, Velocity_df, True)
    print rhodm_avg
    print rhodm_RSD.sum()/np.float64(nbox)**3.
    rhodm = rhodm_RSD/rhodm_avg 
    print np.amax(rhodm)
    print np.amin(rhodm)
    #outfile="/mnt/data1/sandeep/Simulation_data/"\
    #        "rhodm_data/200Mpc_400_RSD/test_rhodm_%dMpc_%d_%d.npy"%(Lbox, np.int32(nbox), snapnumber)
    outfile=dir_name+"rhodm_snapshot/%dMpc_%d_RSD/rhodm_%dMpc_%d_%d.npy"%(Lbox, npart_sim, Lbox, np.int32(nbox), snapnumber)
    np.save(outfile, rhodm)
    Sim_pow = Gen_Power_Sim.Sim_Power(np.int32(nbox), Lbox, snapnumber, npart_sim)
    Sim_pow.setk()
    Sim_pow.setweights()
    Sim_pow.setdeconv()
    Sim_pow.compute_3dpower(rhodm, 30, True)

if key==2:

    CIC_object = smoothden.SmoothDensity(snapshot, np.int32(nbox))
    rhodm_2D, rhodm_avg = CIC_object.SmoothDen2d(Positions_df, Velocity_df)
    print ""
    print "CIC in x-z plane if it is in different place please change the CIC_dim key"
    print rhodm_avg
    print rhodm_2D.sum()/np.float64(nbox)**2.
    rhodm = rhodm_2D/rhodm_avg 
    print np.amax(rhodm)
    print np.amin(rhodm)
    outfile="/mnt/data1/sandeep/Simulation_data/"\
            "rhodm_data/117_REAL/test_rhodm_2D_x-z_%dMpc_%d.npy"%(Lbox, np.int32(nbox))
    np.save(outfile, rhodm)


if key==3:
    CIC_object = smoothden.SmoothDensity(snapshot, Positions_df, np.int32(nbox), True, True)
    rhodm_2D_RSD = np.zeros((np.int32(nbox), np.int32(nbox)), dtype=np.float64) 
    print ""
    print "CIC in x-z plane if it is in different place please change the CIC_dim key"
    CIC_object.SmoothDen(rhodm_2D_RSD, 2)
    print rhodm_avg
    print rhodm_2D_RSD.sum()/np.float64(nbox)**2.
    rhodm_2D_RSD = rhodm_2D_RSD/rhodm_avg 
    print np.amax(rhodm_2D_RSD)
    print np.amin(rhodm_2D_RSD)

    print np.amin(np.log10(rhodm))
    print np.amax(np.log10(rhodm))
    
"""





