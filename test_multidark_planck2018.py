import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import sys
import matplotlib.gridspec as gridspec
import Cosmo_Growth_Fac_raw as CGF_raw
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.colors import ListedColormap
from astropy.cosmology import LambdaCDM
import readsnap
from lmfit.models import LinearModel

mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['font.sans-serif'] = ['Tahoma']
mpl.rcParams['text.usetex'] = True
plt.style.use("classic")


# First analysis
#------------ input Params ----------------------

# Remember nbox is ngrid not box size its Lbox

Lbox = np.int32(sys.argv[1])
nbox = np.int32(sys.argv[2])
npart_sim = np.int32(sys.argv[3])

#
#------------------------------------------------
dir_name = "/mnt/data1/sandeep/New_Gadget2_run/%dMpc_%d/"%(Lbox, npart_sim)
#inp_file = dir_name + "snapdir_seed_1690811/snapshot_005.0"
inp_file = dir_name + "snapdir_seed_1690811/snapshot_005.0"
print inp_file 
snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
#    print "============Gadget header================"
print snapshot.read_gadget_header()

OmegaMatter = snapshot.Omega0
OmegaLambda = snapshot.OmegaLambda
H0 = snapshot.HubbleParam*100.


#----------------------------------------------------------------------------------

dirc_name = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/"\
            "%dMpc_%d_analysis/power_spec_data_1690811/"%(Lbox, npart_sim)
#fin = dirc_name+"3dpower_dm_%dMpc_%d_5.txt"%(Lbox, nbox)
fin = dirc_name+"3dpower_dm_%dMpc_%d_5_RSD.txt"%(Lbox, nbox)
K1     = np.genfromtxt(fin, usecols=0)

model = CGF_raw.Growth_Factor(h=snapshot.HubbleParam, Om=OmegaMatter, 
                               Onu=OmegaLambda, amin=1e-5, dlna=0.0001)

bias=1
snapshots = np.array([5, 4, 3, 2, 1, 0])
snapshots_redshift = np.array([0.00, 1.0, 2.0, 3.0, 4.0, 5.0])
Delta1 = np.zeros((len(snapshots), len(K1)), dtype=np.float64)
Delta1_RSD = np.zeros((len(snapshots), len(K1)), dtype=np.float64)
err = np.zeros(len(snapshots), dtype=np.float64)
err = np.genfromtxt(fin, usecols=2)
factor_0 = np.zeros((len(snapshots)), dtype=np.float64)
count=0
for num in snapshots:
    D = model.growth_factor_fnc(snapshots_redshift[count])    
    beta = model.growth_rate_fnc(snapshots_redshift[count])/bias
    factor_0[count] = 1+(2.*beta/3) +(1.*beta**2/5)
    fin = dirc_name+"3dpower_dm_%dMpc_%d_%d.txt"%(Lbox, nbox, num)
    Delta1[count,:] = np.genfromtxt(fin, usecols=1)/ D**2
    fin = dirc_name+"3dpower_dm_%dMpc_%d_%d_RSD.txt"%(Lbox, nbox, num)
    Delta1_RSD[count,:] = np.genfromtxt(fin, usecols=1)/ D**2

    count+=1


dirc_name = "/mnt/data1/sandeep/New_Gadget2_run/Cosmology/"
CAMB_K     = np.genfromtxt(dirc_name+"multidark2_pofk_z0.dat", usecols=0)
CAMB_Pk    = np.genfromtxt(dirc_name+"multidark2_pofk_z0.dat", usecols=1)
Sigma_multidark= 0.8228
Fname = "multidark2_pofk_z0_norm.dat"
norm_Pk1 = model.compute_sigma_sqr(CAMB_Pk, CAMB_K, Sigma_multidark, Fname)

colors = ['r','g','b','c','m', 'y']

#----------------------------------------------------------------------------------
"""
fig= plt.figure(1, figsize=(10, 8))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])
#NUM_COLORS = len(snapshots)
#cm = plt.get_cmap('gist_rainbow')
#ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])
for i in xrange(len(snapshots)):
    ax1.plot(np.log10(K1), np.log10(Delta1[i,:]),linestyle='', 
            marker='*', mfc='none', mew=1.8, ms=10, linewidth=2.5, mec=colors[i],
            label=r"$\rm{P}^{%0.2f}_{m}(\rm{k})$"%snapshots_redshift[i])

ax1.plot((CAMB_K), np.log10(norm_Pk1),linestyle='--', marker='', 
         mfc='none', mew=1.8, color='k', ms=10, linewidth=2.5, 
         label=r"$\rm{P}^{\rm{CAMB}}_{m}(\rm{k})$")

#ax1.set_title(r"$1\rm{Gpc},\rm{Ngrid}:1024, REAL$", fontsize=16)
ax1.set_title(r"$%d\rm{Mpc},\rm{Ngrid}:1024, REAL$"%Lbox, fontsize=16)
ax1.set_xlabel(r'$\rm{log}_{10}(\rm{k})$', fontsize=20)
ax1.set_ylabel(r'$\rm{log}_{10}(\Delta^2)$', fontsize=20)
#ax1.set_ylim(-1.5, 1.0)
#ax1.set_xlim(-1.7, 0.0)
ax1.set_ylim(-4, 3.5)
ax1.set_xlim(-2.5, 2)

ax1.minorticks_on()
#ax1.legend(frameon=True, ncol=1, fontsize=12, loc='center left', bbox_to_anchor=(1, 0.5) )
ax1.legend(frameon=True, ncol=1, fontsize=16, loc=4)
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
plt.tight_layout()
plot_name = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/New_run_plots/"
fout = plot_name+"MultiSnap_Common_PowSpec_%dMpc_%d_zoom_RSD.png"%(Lbox, npart_sim)
fig.savefig(fout, bbox_inches='tight')
"""



fig= plt.figure(1, figsize=(10, 8))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])
#ax1.plot(np.log10(K1), (Delta1_RSD[0,:]/Delta1[0,:]),linestyle='', 
#        marker='*', mfc='none', mew=1.8, ms=10, linewidth=2.5, mec=colors[0],
#        label=r"$\rm{P}^{%0.2f}_{m}(\rm{k})$"%snapshots_redshift[0])

ax1.errorbar(K1, Delta1_RSD[0,:]/Delta1[0,:], yerr=err, fmt='o', ecolor='k', 
             elinewidth=2.5, capsize=8, mec='blue',
             ms=8, mfc='none', mew=2.0, label=r"")

ax1.axhline(y= (Delta1_RSD[0,:]/Delta1[0,:])[0], linestyle='--', linewidth=2.5, 
            color='r', label=r"$\rm{f}_{\rm{fit}}$:%0.4f"%(Delta1_RSD[0,:]/Delta1[0,:])[0])
ax1.axhline(y=factor_0[0], linestyle='--', linewidth=2.5, color='g', 
            label=r"$\rm{f}_{\rm{theory}}$:%0.4f"%factor_0[0])

ax1.set_title(r"$%d\rm{Mpc},\rm{Ngrid}:1024, REAL$"%Lbox, fontsize=16)
ax1.set_xlabel(r'$\rm{k}[\rm{h}^{-1} \rm{Mpc}]$', fontsize=20)
ax1.set_ylabel(r'$\rm{P}_{s}(\rm{k})/\rm{P}_{r}(\rm{k}) $', fontsize=20)
#ax1.set_ylim(-4, 3.5)
#ax1.set_xlim(-2.5, 2)
ax1.set_ylim(0.2, 10**0.6)
ax1.set_xscale("log")
ax1.set_yscale("log")

ax1.minorticks_on()
ax1.legend(frameon=False, ncol=1, fontsize=16, loc=3)
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
plt.tight_layout()
#plot_name = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/New_run_plots/"
#fout = plot_name+"MultiSnap_Common_PowSpec_%dMpc_%d_zoom_RSD.png"%(Lbox, npart_sim)
#fig.savefig(fout, bbox_inches='tight')

plt.show()

"""
count = 0
fig= plt.figure(3, figsize=(22, 18))
gs = gridspec.GridSpec(3, 3, hspace=0.4, wspace=0.4)

for i in xrange(0, 3):
    for j in xrange(0, 2):
        
        ax1 = plt.subplot(gs[i, j])
        ax1.errorbar(K2, Delta1_RSD[count,:]/Delta1[count,:], yerr=err, fmt='o', ecolor='k', 
                     elinewidth=2.5, capsize=8, mec='blue',
                     ms=8, mfc='none', mew=2.0, label=r"")
                     
        ax1.axhline(y= (Delta2_RSD[count,:]/Delta2[count,:])[0], linestyle='--', linewidth=2.5, 
                    color='r', label=r"$\rm{f}_{\rm{fit}}$:%0.4f"%(Delta2_RSD[count,:]/Delta2[count,:])[0])
                    
        ax1.axhline(y=factor_0[count], linestyle='--', linewidth=2.5, color='g', 
                    label=r"$\rm{f}_{\rm{theory}}$:%0.4f"%factor_0[count])

        ax1.set_title(r"$z=%0.2f$"%snapshots_redshift[count], fontsize=16)
        ax1.set_xscale("log")
        ax1.set_yscale("log")
        ax1.set_xlabel(r'$\rm{k}(\rm{h}/\rm{Mpc})$', fontsize=22)
        ax1.set_ylabel(r'$\Delta^2_{\rm{s}}/\Delta^2_{\rm{r}}$', fontsize=22)

        ax1.set_ylim(10**-1.5, 10**0.6)
#        ax1.set_xlim(, 0.2)
        ax1.minorticks_on()
        ax1.legend(frameon=False, loc=3, ncol=1, fontsize=20 )
        ax1.tick_params(axis='both', which='minor', length=5,
                              width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
                               width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.8)
        ax1.spines['left'].set_linewidth(1.8)
        ax1.spines['top'].set_linewidth(1.8)
        ax1.spines['right'].set_linewidth(1.8)
        count+=1

plt.tight_layout()
#plot_name = "/mnt/data1/sandeep/Simulation_data/plots/"
plot_name = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/"\
            "%dMpc_%d_analysis/plots/"%(Lbox, npart_sim)
fout = plot_name+"PowSpec_Ratio_Diff_Snapshot_%dMpc_%d_fit.png"%(Lbox, npart_sim)
fig.savefig(fout, bbox_inches='tight')
"""



""" 
#Second analysis
#Lbox = [1000, 800, 400,200]
Lbox = [200, 400, 800, 1000]
nbox = np.int32(sys.argv[1])
npart_sim = np.int32(sys.argv[2])


dir_name = "/mnt/data1/sandeep/New_Gadget2_run/%dMpc_%d/"%(Lbox[0], npart_sim)
inp_file = dir_name + "snapdir_seed_1690811/snapshot_005.0"
print inp_file 
snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
#    print snapshot.read_gadget_header()
OmegaMatter = snapshot.Omega0
OmegaLambda = snapshot.OmegaLambda
H0 = snapshot.HubbleParam*100.
model = CGF_raw.Growth_Factor(h=snapshot.HubbleParam, Om=OmegaMatter, 
                               Onu=OmegaLambda, amin=1e-5, dlna=0.0001)
D = model.growth_factor_fnc(snapshot.redshift)    
#------------------------------------------------

dirc_name = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/"\
            "%dMpc_%d_analysis/power_spec_data_1690811/"%(Lbox[0], npart_sim)
fin = dirc_name+"3dpower_dm_%dMpc_%d_5_RSD.txt"%(Lbox[0], nbox)
print fin
k_mode_200 = np.genfromtxt(fin, usecols=0)
Delta_200 = np.genfromtxt(fin, usecols=1)/D**2


dirc_name = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/"\
            "%dMpc_%d_analysis/power_spec_data_1690811/"%(Lbox[1], npart_sim)
fin = dirc_name+"3dpower_dm_%dMpc_%d_5_RSD.txt"%(Lbox[1], nbox)
print fin
k_mode_400 = np.genfromtxt(fin, usecols=0)
Delta_400 = np.genfromtxt(fin, usecols=1)/D**2



dirc_name = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/"\
            "%dMpc_%d_analysis/power_spec_data_1690811/"%(Lbox[2], npart_sim)
fin = dirc_name+"3dpower_dm_%dMpc_%d_5_RSD.txt"%(Lbox[2], nbox)
print fin
k_mode_800 = np.genfromtxt(fin, usecols=0)
Delta_800 = np.genfromtxt(fin, usecols=1)/D**2


dirc_name = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/"\
            "%dMpc_%d_analysis/power_spec_data_1690811/"%(Lbox[3], npart_sim)
#fin = dirc_name+"3dpower_dm_%dMpc_%d_5.txt"%(Lbox[3], nbox)
fin = dirc_name+"3dpower_dm_%dMpc_%d_5_RSD.txt"%(Lbox[3], nbox)
print fin
k_mode_1000 = np.genfromtxt(fin, usecols=0)
Delta_1000 = np.genfromtxt(fin, usecols=1)/D**2


#------------------------------------------------

dirc_name = "/mnt/data1/sandeep/New_Gadget2_run/Cosmology/"
CAMB_K     = np.genfromtxt(dirc_name+"multidark2_pofk_z0.dat", usecols=0)
CAMB_Pk    = np.genfromtxt(dirc_name+"multidark2_pofk_z0.dat", usecols=1)
Sigma_multidark= 0.8228
Fname = "multidark2_pofk_z0_norm.dat"
norm_Pk1 = model.compute_sigma_sqr(CAMB_Pk, CAMB_K, Sigma_multidark, Fname)

#----------------------------------------------------------------------------------

colors = ['r','g','b','m', 'y']
fig= plt.figure(1, figsize=(10, 8))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])


ax1.plot(np.log10(k_mode_200), np.log10(Delta_200),linestyle='', 
        marker='*', mfc='none', mew=1.8, ms=10, linewidth=2.5, mec=colors[0],
        label=r"%d$\rm{Mpc}$"%Lbox[0])


ax1.plot(np.log10(k_mode_400), np.log10(Delta_400),linestyle='', 
        marker='o', mfc='none', mew=1.8, ms=10, linewidth=2.5, mec=colors[1],
        label=r"%d$\rm{Mpc}$"%Lbox[1])


ax1.plot(np.log10(k_mode_800), np.log10(Delta_800),linestyle='', 
        marker='<', mfc='none', mew=1.8, ms=10, linewidth=2.5, mec=colors[2],
        label=r"%d$\rm{Mpc}$"%Lbox[2])

ax1.plot(np.log10(k_mode_1000), np.log10(Delta_1000),linestyle='', 
        marker='D', mfc='none', mew=1.8, ms=10, linewidth=2.5, mec=colors[3],
        label=r"%d$\rm{Mpc}$"%Lbox[3])


ax1.plot((CAMB_K), np.log10(norm_Pk1),linestyle='--', marker='', 
         mfc='none', mew=1.8, color='k', ms=10, linewidth=2.5, 
         label=r"$\rm{P}^{\rm{CAMB}}_{m}(\rm{k})$")

ax1.set_xlabel(r'$\rm{log}_{10}(\rm{k})$', fontsize=20)
ax1.set_ylabel(r'$\rm{log}_{10}(\Delta^2)$', fontsize=20)
ax1.set_ylim(-4, 1.)
ax1.set_xlim(-2.3, -0.5)

ax1.minorticks_on()
ax1.legend(frameon=True, ncol=1, fontsize=16, loc=4)
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
plt.tight_layout()
plot_name = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/New_run_plots/"
fout = plot_name+"Common_PowSpec_%d_%d_zoom_RSD.png"%( nbox, npart_sim)
fig.savefig(fout, bbox_inches='tight')
"""















