import numpy as np
import Cosmo_Growth_Fac_raw as CGF_raw
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
from astropy.cosmology import LambdaCDM
from matplotlib import rcParams
import matplotlib as mpl
from lmfit.models import LinearModel

rcParams['font.family'] = 'sans-serif'
rcParams['font.sans-serif'] = ['Tahoma']
mpl.rcParams['text.usetex'] = True
plt.style.use("classic")

OmegaMatter = 0.3
OmegaLambda = 0.7
h = 0.7
H0 = h*100.

Omega0 = OmegaMatter+OmegaLambda

def Hz(zepoch):
    return H0 * np.sqrt(OmegaMatter*(1+zepoch)**3+
                        (1-Omega0)*(1+zepoch)**2 +
                         OmegaLambda)

def Omega_z(y):
    return  OmegaMatter * (1.+y)**(3)  * H0**2 / Hz(y)**2
    



model = CGF_raw.Growth_Factor(h=0.7, Om=0.3, Onu=0.7, amin=1e-5, dlna=0.01)

delta_plus_a_norm = model.growth_factor(0.0)

f = model.growth_rate(0.)

f1 = model.growth_rate_analytical(0., True)

scale_factor = np.exp(model.lna)

z = 1.0 / scale_factor 
z -=1
OmegaM = model.cosmo.Om(z)


#==================================================



fig = plt.figure(1, figsize=(8, 6.5))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0]) 
ax1.plot(scale_factor, delta_plus_a_norm, linestyle='-', color="green",
         linewidth=2, label=r'$\Lambda\rm{CDM}$')

ax1.set_xlim(np.max(scale_factor), 10**(-5.1))
ax1.set_ylim(10**(-6), 1.)
ax1.set_yscale("log")
ax1.set_xscale("log")
ax1.set_ylabel(r"$\delta_{+}/\delta_{+}^{0}$", fontsize=24)
ax1.set_xlabel(r"$\mathrm{a}$", fontsize=24)
ax1.minorticks_on()
ax1.legend(loc=1, frameon=False, ncol=1, fontsize=20)
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
plt.tight_layout()
plt.savefig("Normalized_GrowFac.pdf",  dpi=300,)
#=====================================================
fig = plt.figure(2, figsize=(8, 6.5))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0]) 

ax1.plot(scale_factor, f,linestyle='-', color="red",
         linewidth=2, label=r'$\rm{f}_{\rm{numerical}}$')
ax1.plot(scale_factor, f1,linestyle='-.', color="green",
         lw=2, label=r'$\rm{f}_{\rm{analytical}}$')


ax1.set_xlim(np.max(scale_factor), 10**(-5.1))
ax1.set_yscale("log")
ax1.set_xscale("log")

ax1.set_ylabel(r"$\rm{d} \rm{ln\bar{d}}/\rm{d} \rm{lna}$", fontsize=24)
ax1.set_xlabel(r"$\rm{a}$", fontsize=24)
ax1.minorticks_on()
ax1.legend(loc=2, frameon=False, ncol=1, fontsize=20)
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
plt.tight_layout()
plt.savefig("Growth_Rate.pdf",  dpi=300,)


#==================================================




ind = (f==np.inf)
f =f[~ind] 
OmegaM = OmegaM[~ind]
ind = (OmegaM<=0.999)
f =f[ind] 
OmegaM = OmegaM[ind]

print max(OmegaM)
y = np.log10(f)

x=np.log10(OmegaM)
mod = LinearModel()
pars = mod.guess(y, x=x)
out = mod.fit(y, pars, x=x)
print(out.fit_report())

b = out.params['slope'].value
a = out.params['intercept'].value

fig = plt.figure(3, figsize=(8, 6.5))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0]) 

ax1.plot(OmegaM, f,marker='o', mfc='none', mew=2, mec='blue', ms=10, 
        linestyle='', label='')
ax1.plot(OmegaM, 10**a * OmegaM**b, linestyle='-',
        lw=2, color="red", label=r'$\gamma:%0.4f$'%(b))

ax1.set_ylim(0.4, 1.2)
ax1.set_xlim(0.3, 0.98)

ax1.set_ylabel(r"$\rm{d} \rm{ln\bar{d}}/\rm{d} \rm{lna}$", fontsize=24)
ax1.set_xlabel(r"$\Omega_{\rm{M}}$", fontsize=24)
ax1.minorticks_on()
ax1.legend(loc=4, frameon=False, ncol=1, fontsize=20)
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
plt.tight_layout()

plt.savefig("Growth_Rate_Omega_M_relation.pdf",  dpi=300,)



plt.show()



