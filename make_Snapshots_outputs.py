
#-------------------------------------------------------------------
#	Written by:	Sandeep Rana
#	Last Edited:	12 March 2019
#	Descrition:	specify range in redshift [zi,zf] --> both ends included
#			the number of bins
#			the type of bins , normal or log
#			It will print on stdout the scale factors
#            bit modified from Nishi's code as here you have to specify the plan
#--------------------------------------------------------------------

import numpy as np
import sys
import os


print ""
print "======================================"
print ("This simple routine will create"\
       "outputs.txt file that have scale "\
       "factor according to your plan "\
       "when done exit")
print "======================================"

plan = "Y"
counter = 0
while plan=='Y':
    
    print ""
    print "Enter initial redshift for e.g. z=150"
    zedi = np.float64(raw_input(""))
    print ""
    print "Enter final redshift for e.g. z=0.1"
    zedf = np.float64(raw_input(""))
    
    if zedf>=zedi:
        print "Erro: final redshift is larger than initial......exitint"
        sys.exit()

    print "Specify number of bins"
    nbins = np.int32(raw_input(""))
    print "Do you want bins logspace(type 1) or linear space"
    bin_space = np.int32(raw_input(""))

    if counter==0:
        if bin_space==1:
            temp = np.logspace(np.log10(zedf), np.log10(zedi), nbins, endpoint=True)
            scale_fac = 1./(1.+temp)
        else:
            temp = np.linspace(zedf, zedi, nbins, endpoint=True)
            scale_fac = 1./(1.+temp)
        counter+=1
    else:
        if bin_space==1:
            temp = np.logspace(np.log10(zedf), np.log10(zedi), nbins, endpoint=True)
            new_scale_fac = 1./(1.+temp)
            scale_fac = np.append(scale_fac, new_scale_fac)
        else:
            temp = np.linspace(zedf, zedi, nbins, endpoint=True)
            new_scale_fac = 1./(1.+temp)
            scale_fac = np.append(scale_fac, new_scale_fac)

    print "do you want to plan more [Y/N]"
    plan=raw_input("")
    




temp1 = np.unique(scale_fac)
np.savetxt("outputs_1.txt", temp1, fmt="%0.8f", delimiter='\t')

