
"""
Dark Matter only visualization 
with different switchs (here small and large
depends upon you system size and capability
MPI implimention is yet to come) 
There is a better public code gaepsi available by Yu Feng
https://github.com/rainwoodman/gaepsi 
Used in Massive Black II visualization
Need to add it in future.

1. Small File 2D CIC REAL Field Visualization
2. Small File 2D CIC RSD  Field Visualization
3. Large File 2D CIC REAL Field Visualization
3. Large File 2D CIC RSD Field Visualization
"""

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib as mpl 
import time
import itertools
import scipy.ndimage as ndimage
from matplotlib.colors import LogNorm
from matplotlib.colors import ListedColormap
from matplotlib.colors import LinearSegmentedColormap
from astropy.cosmology import LambdaCDM
import matplotlib.patches as patches

from numba import njit
import os
import sys
from  norm_color import *
from my_colormap import *
import argparse
sys.path.insert(1, '/mnt/home1/sandeep/workspace/Simulations_workspace/python_routines/Gadget2_routines/')
import readsnap
import smoothden
#mpl.rcParams['font.family'] = 'sans-serif'
#mpl.rcParams['font.sans-serif'] = ['Tahoma']
mpl.rcParams['text.usetex'] = True
#plt.style.use("seaborn-dark-palette")
plt.style.use("seaborn-talk")
from mpl_toolkits.axes_grid1.inset_locator import zoomed_inset_axes
from mpl_toolkits.axes_grid.inset_locator import inset_axes
from mpl_toolkits.axes_grid1.inset_locator import mark_inset
from matplotlib.offsetbox import AnchoredText
from mpl_toolkits.axes_grid1.anchored_artists import AnchoredSizeBar

from sphviewer.tools import QuickView
from sphviewer.tools.cmaps import *

#-------------------------------------------------------------------------------------------------
@njit
def TSC_interpolation(Nbox, NpartTotal, masspart, Pos1):
    """
    """
    
    
    denpart = np.zeros((Nbox, Nbox, Nbox), dtype=np.float64)
    
    TSC_W_x = np.zeros(3 , dtype=np.float64)
    TSC_W_y = np.zeros(3 , dtype=np.float64)
    TSC_W_z = np.zeros(3 , dtype=np.float64)
    
    #Compute CIC weights

    index_x = np.zeros(3 , dtype=np.int32)
    index_y = np.zeros(3 , dtype=np.int32)
    index_z = np.zeros(3 , dtype=np.int32)


        
    for i in xrange(NpartTotal):
        
        for j in xrange(-1, 2):
            
            intpos = np.rint(Pos1[i, 0])
            diff = np.fabs(intpos+j - Pos1[i, 0])
            index_x[j+1] = (intpos + j)%Nbox 

            if   j==-1: 
                 TSC_W_x[j+1] =  0.5 * (1.5-diff) * (1.5-diff)
            elif j==0: 
                 TSC_W_x[j+1] =  0.75 - diff * diff
            else:  
                 TSC_W_x[j+1] =  0.5 * (1.5-diff) * (1.5-diff)

            
            intpos = np.rint(Pos1[i, 1])
            diff = np.fabs(intpos+j - Pos1[i, 1])
            index_y[j+1] = (intpos + j)%Nbox 

            if   j==-1: 
                 TSC_W_y[j+1] =  0.5 * (1.5-diff) * (1.5-diff)
            elif j==0: 
                 TSC_W_y[j+1] =  0.75 - diff * diff
            else:  
                 TSC_W_y[j+1] =  0.5 * (1.5-diff) * (1.5-diff)


            intpos = np.rint(Pos1[i, 2])+0.5
            diff = np.fabs(intpos+j - Pos1[i, 2])
            index_z[j+1] = (intpos + j)%Nbox 

            if   j==-1: 
                 TSC_W_z[j+1] =  0.5 * (1.5-diff) * (1.5-diff)
            elif j==0: 
                 TSC_W_z[j+1] =  0.75 - diff*diff
            else:  
                 TSC_W_z[j+1] =  0.5 * (1.5-diff) * (1.5-diff)


        for l in xrange(3):  
            for m in xrange(3):  
                for n in xrange(3):
                    denpart[index_x[l], index_y[m], index_z[n]] += (np.float64(masspart) 
                                                                    * TSC_W_x[l] * TSC_W_y[m]* TSC_W_z[n])
       
        
    return denpart 


#***********************************************************************************

@njit
def SDen2D(ind1, ind2, cic_w_1, cic_w_2,
           masspart, npart, denpart, shiftlist):

    for i in xrange(npart):
        for j in xrange(len(shiftlist)):
            i1, i2  = shiftlist[j]
            j1 = ind1[i, i1]
            j2 = ind2[i, i2]
            denpart[j1][j2] = denpart[j1][j2] + masspart*cic_w_1[i, i1]*cic_w_2[i, i2]
    return denpart


def CIC_component(Nbox, NpartTotal, Pos1):
        
    ints = np.floor(Pos1).astype(np.int32)
    ints_x = ints[:,0]
    ints_y = ints[:,1]
    ints_z = ints[:,2]

    j1_ind = np.zeros((NpartTotal,2) , dtype=np.int32)
    j2_ind = np.zeros((NpartTotal,2) , dtype=np.int32)
    j3_ind = np.zeros((NpartTotal,2) , dtype=np.int32)
    for i in xrange(0, 2):
        j1_ind[:, i] = (i+ints_x)%Nbox
        j2_ind[:, i] = (i+ints_y)%Nbox
        j3_ind[:, i] = (i+ints_z)%Nbox
    
    frac = np.modf(Pos1)[0]
    remfrac = 1 - frac

    return j1_ind, j2_ind, j3_ind, frac, remfrac


def Compute_CIC_weight(frac, remfrac, NpartTotal):
    """
    Compute CIC weights
    """
    CIC_W_x = np.zeros((NpartTotal,2) , dtype=np.float64)
    CIC_W_y = np.zeros((NpartTotal,2) , dtype=np.float64)
    CIC_W_z = np.zeros((NpartTotal,2) , dtype=np.float64)
    
    CIC_W_x[:,1] = frac[:,0]
    CIC_W_x[:,0] = remfrac[:,0]
    CIC_W_y[:,1] = frac[:,1]
    CIC_W_y[:,0] = remfrac[:,1]
    CIC_W_z[:,1] = frac[:,2]
    CIC_W_z[:,0] = remfrac[:,2]
    
    return CIC_W_x, CIC_W_y, CIC_W_z


#***********************************************************************************

def small_2D_real_cic(inp_file, Lbox, nbox, nfiles, npart_sim, snapID, dir_name):
    
    for i in xrange(nfiles):
        if nfiles<=1: 
            inp_file = dir_name + "snapdir/snapshot_%03d"%(snapID)
        else:
            inp_file = dir_name + "snapdir/snapshot_%03d.%d"%(snapID, i)
            
        snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
        if i==0:
            Positions_df  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
            Positions_df.columns=['x','y','z']
            Velocity_df   = snapshot.read_Vel()
            Velocity_df.columns=['vx','vy','vz']

        if i==1:
            Positions_df1  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
            Positions_df1.columns=['x','y','z']
            Velocity_df1   = snapshot.read_Vel()
            Velocity_df1.columns=['vx','vy','vz']

        if i==2:
            Positions_df2  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
            Positions_df2.columns=['x','y','z']
            Velocity_df2   = snapshot.read_Vel()
            Velocity_df2.columns=['vx','vy','vz']

        if i==3:
            Positions_df3  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
            Positions_df3.columns=['x','y','z']
            Velocity_df3   = snapshot.read_Vel()
            Velocity_df3.columns=['vx','vy','vz']


# -------------------------Merging all data frames---------------
    if nfiles>1:
        tmp = Positions_df.append(Positions_df1, ignore_index=True, sort=False)
        tmp1 = tmp.append(Positions_df2, ignore_index=True, sort=False)
        tmp2 = tmp1.append(Positions_df3, ignore_index=True, sort=False)
        Positions_df = tmp2

        del Positions_df1
        del Positions_df2 
        del Positions_df3

# ===Merging all Vel data frames============================

        tmp = Velocity_df.append(Velocity_df1, ignore_index=True, sort=False)
        tmp1 = tmp.append(Velocity_df2, ignore_index=True, sort=False)
        tmp2 = tmp1.append(Velocity_df3, ignore_index=True, sort=False)
        Velocity_df = tmp2

        del Velocity_df1
        del Velocity_df2
        del Velocity_df3

    print "\n"
    
    CIC_object = smoothden.SmoothDensity(snapshot, np.int32(nbox))
    rhodm_2D, rhodm_avg, slice_width, boxfac, pos = CIC_object.SmoothDen2d(Positions_df, Velocity_df)
    
    print "CIC in x-z plane if it is in different place please change the CIC_dim key"
    print "rhodm_avg:", rhodm_avg
    print "rhodm avg  CIC", (slice_width*boxfac)*rhodm_2D.sum()/np.float64(nbox)**2.
    rhodm = rhodm_2D/rhodm_avg 
    print"\n"
    print "max rhodm:", np.amax(rhodm)
    print "min rhodm:", np.amin(rhodm)
    
    outfile=("rhodm_x-z_%03dMpc_%d_%d.npy"%(Lbox, np.int32(nbox), npart_sim))
    np.save(outfile, rhodm)
    #rhodm.tofile(outfile)
    
    ind = (rhodm==np.amin(rhodm))
    value = np.amin(rhodm[rhodm!=np.amin(rhodm)])
    rhodm[ind] = value
    print np.min(rhodm)

    fig = plt.figure(1, figsize=(15, 15)) 
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0]) 
    
    img =  ndimage.gaussian_filter(rhodm, sigma=1.0, mode='wrap', truncate=2)
    print"max Position\n"

    print  ndimage.maximum_position(rhodm, labels=None, index=None)
    im = ax1.imshow(img.T, origin= 'lower', cmap=twilight(),#twilight(), 
                    extent=[0, np.int32(snapshot.BoxSize/1000.), 0,  
                    np.int32(snapshot.BoxSize/1000.)], interpolation='none', 
                    norm = LogNorm(vmin=0.002, vmax=10) )


    ax1.set_title("Real Space")
    ax1.set_ylabel(r"$\mathrm{z}(\rm{Mpc}$ ${h}^{-1})$", fontsize=20)
    ax1.set_xlabel(r"$\mathrm{x}(\rm{Mpc}$ ${h}^{-1})$", fontsize=20)
    ax1.minorticks_on()
    ax1.legend(loc=4, frameon=False, ncol=1, fontsize=14)
    ax1.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.5)
    ax1.spines['left'].set_linewidth(1.5)
    ax1.spines['top'].set_linewidth(1.5)
    ax1.spines['right'].set_linewidth(1.5)
    plt.tight_layout()
    cbar = plt.colorbar(im, fraction=0.046, pad=0.04)
    cbar.set_label(r"$\rho/\bar{\rho}$",fontsize=20)

    plt.savefig("rho_original_20Mpc_Slice_%d_%d_%d_twilight.png"%(int(Lbox), 
                snapID, int(npart_sim)), bbox_inches='tight', dpi=300) 



    Pos2D = np.zeros(((pos.shape)[0], 3), dtype=np.float64) 
    
    Pos2D[:,0] = pos[:,0]/boxfac/1000.0
    Pos2D[:,1] = pos[:,2]/boxfac/1000.0
    Pos2D[:,2] = pos[:,1]/boxfac/1000.0

    max_corrd =  np.float64(ndimage.maximum_position(rhodm, labels=None, index=None))#/boxfac/1000.
    print max_corrd    
    print max_corrd/boxfac/1000.0
    
    scaled_max_corrd= max_corrd/boxfac/1000.0
    lbox_half = Lbox/2.    
    xshift = lbox_half-scaled_max_corrd[0]
    if xshift<0:
        xshift=-np.abs(xshift)
    else:
        xshift =np.abs(xshift)

    yshift = lbox_half-scaled_max_corrd[1]
    if yshift<0:
        yshift= -np.abs(yshift)
    else:
        yshift = np.abs(yshift)

    print xshift, yshift
    print xshift+scaled_max_corrd[0], yshift+scaled_max_corrd[1]
    
    Pos2D[:, 0] = (Pos2D[:, 0] + xshift)%Lbox
    Pos2D[:, 1] = (Pos2D[:, 1] + yshift)%Lbox


    indx = (Pos2D[:, 0] <110.0) * (Pos2D[:,0]>=90.0)
    tempx = Pos2D[indx, 0] 
    tempy = Pos2D[indx, 1]
    tempz = Pos2D[indx, 2] 

    indy = (tempy <110.0) * (tempy>=90.0)
    
    tempxx = tempx[indy]
    tempyy = tempy[indy]
    tempzz = tempz[indy]

    tempxx = tempxx-90
    tempyy = tempyy-90
    tempzz = tempzz-90

    del Pos2D

    npartTotal = len(tempxx)
    Pos = np.zeros((len(tempxx), 3), dtype=np.float64) 
    zoom_BoxSize=20000. #Mpc 
    nbox_zoom = 400.0
    boxfac = np.float64(nbox_zoom/zoom_BoxSize)
    Pos[:, 0] = tempxx*boxfac*1000
    Pos[:, 1] = tempyy*boxfac*1000
    Pos[:, 2] = tempzz*boxfac*1000
            
    
    j1_ind, j2_ind, j3_ind, frac, remfrac = CIC_component(nbox_zoom, npartTotal, Pos)   
    
    slist_2D = list(itertools.product([0,1],repeat=2))
    slist_2D = np.asarray(slist_2D)

    CIC_W_x, CIC_W_y, CIC_W_z = Compute_CIC_weight(frac, remfrac, npartTotal)
    
    ParticleMass =snapshot.massarr[1]
    print "nbox\n"
    print int(nbox)
    Density = np.zeros((int(nbox_zoom), int(nbox_zoom)), dtype=np.float64)
    SDensity = SDen2D(j1_ind, j2_ind, 
                CIC_W_x, CIC_W_y, 
                np.float64(ParticleMass), 
                np.int32(npartTotal), Density, slist_2D)

    rhodm_avg = rhodm_2D.sum()/np.float64(nbox_zoom)**2.
    zoom_rhodm = SDensity/rhodm_avg
    outfile=("max_rhodm_x-z_%03dMpc_%d_%d_zoom.npy"%(Lbox, np.int32(nbox_zoom), npart_sim))
    np.save(outfile, zoom_rhodm)


    ind = (zoom_rhodm==np.amin(zoom_rhodm))
    value = np.amin(zoom_rhodm[zoom_rhodm!=np.amin(zoom_rhodm)])
    zoom_rhodm[ind] = value
    
    print"Minimum\n"
    
    print np.min(zoom_rhodm)

#--------------------------------------------------------------

    max_corrd =  ndimage.maximum_position(rhodm, labels=None, index=None)

    center_pix_corrd = nbox/2.
    xshift = center_pix_corrd-max_corrd[0]
    if xshift<0:
        xshift=-np.abs(xshift)
    else:
        xshift =np.abs(xshift)

    yshift = center_pix_corrd-max_corrd[1]
    if yshift<0:
        yshift= -np.abs(yshift)
    else:
        yshift = np.abs(yshift)

    shifts = np.array([xshift, yshift])
    img_shift = np.zeros(rhodm.shape, dtype=np.float64) 
    
    ndimage.shift(rhodm, shifts, output=img_shift, order=3, mode='wrap')
    img =  ndimage.gaussian_filter(img_shift, sigma=1.0, mode='wrap', truncate=2)


    fig, ax1 = plt.subplots(figsize=[15, 15])
    im = ax1.imshow(img.T, origin= 'lower', cmap=twilight(),#twilight()
                    extent=[0, np.int32(snapshot.BoxSize/1000.), 0,  
                    np.int32(snapshot.BoxSize/1000.)], interpolation='none', 
                    norm = LogNorm(vmin=0.002, vmax=10) )


    ax1.set_title("Real Space")
    ax1.set_ylabel(r"$\mathrm{z}(\rm{Mpc}$ ${h}^{-1})$", fontsize=24)
    ax1.set_xlabel(r"$\mathrm{x}(\rm{Mpc}$ ${h}^{-1})$", fontsize=24)
    ax1.minorticks_on()
    ax1.tick_params(axis='both', which='minor', length=5,
                          width=2, labelsize=22)
    ax1.tick_params(axis='both', which='major', length=8,
                           width=2, labelsize=22)
    ax1.spines['bottom'].set_linewidth(2)
    ax1.spines['left'].set_linewidth(2)
    ax1.spines['top'].set_linewidth(2)
    ax1.spines['right'].set_linewidth(2)
    plt.tight_layout()
    cbar = plt.colorbar(im, fraction=0.046, pad=0.04)
    cbar.set_label(r"$\rho/\bar{\rho}$",fontsize=20)


    img_zoom =  ndimage.gaussian_filter(zoom_rhodm, sigma=1.0, mode='wrap', truncate=2)
    print"max Position\n"
    print  ndimage.maximum_position(zoom_rhodm, labels=None, index=None)
  

    axins = inset_axes(ax1, width="30%", # width = 30% of parent_bbox
                            height="30%", # height : 1 inch
                            loc=1)
                            
    axins.imshow(img_zoom.T, origin= 'lower', cmap=twilight(),#twilight()
                    extent=[90, 110, 90, 110],
                    norm = LogNorm(vmin=0.002, vmax=10) )

    x1, x2, y1, y2 = 90, 110, 90, 110

    axins.set_xlim(x1, x2)
    axins.set_ylim(y1, y2)
    
#    at = AnchoredText(r"$20\rm{Mpc}$",
#                  prop=dict(size=15, fontweight="bold", color='r'), frameon=False,
#                  loc='lower right',
#                  )

#    at.patch.set_boxstyle("round,pad=0.,rounding_size=0.2")
#    axins.add_artist(at)

    axins.tick_params(axis='both', which='minor', length=0,
                          width=0, labelsize=22)
    axins.tick_params(axis='both', which='major', length=0,
                          width=0, labelsize=22)
    axins.spines['bottom'].set_linewidth(1.8)
    axins.spines['left'].set_linewidth(1.8)
    axins.spines['top'].set_linewidth(1.8)
    axins.spines['right'].set_linewidth(1.8)
    

    plt.xticks(visible=False)
    plt.yticks(visible=False)
    mark_inset(ax1, axins, loc1=2, loc2=4, fc="none", ec="k", lw=2)
    plt.draw()

    plt.savefig("rho_20Mpc_Slice_%d_%d_%d_twilight.png"%(int(Lbox), snapID, int(npart_sim)), bbox_inches='tight', dpi=300) 
    plt.show()

#-----------------------------------------------------------------------------------------
   

def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("Lbox", type=int, 
    help='BoxSize of the Simulation e.g 200Mpc')
    parser.add_argument("nbox", type=float, 
    help='Grid size for e.g 400, 1024 or 2048')
    parser.add_argument("snapID", type=int, 
    help='Snapshot ID e.g 117, 110, 005')
    parser.add_argument("nfiles", type=int, 
    help='Number of subfiles written by Gadget e.g snapshot.{0-4}')
    parser.add_argument("npart_sim", type=int, 
    help='Npart^3 used in simulation e.g npart_sim =256 for 256^3 particle simulation')
    
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()

#------------------------------------------------
    Lbox = args.Lbox
    nbox = args.nbox
    snapID = args.snapID
    nfiles = args.nfiles
    npart_sim = args.npart_sim
#------------------------------------------------

    dir_name= ("/mnt/data2/nkhandai/new_runs/dmo/Gadget2/planck2018/"\
                "%dMpc_%d/"% (Lbox, npart_sim))
    if nfiles<=1:
        inp_file = dir_name + "snapdir/snapshot_%03d"%(snapID)
    else:
        inp_file = dir_name + "snapdir/snapshot_%03d.%d"%(snapID, 0)

    print"\n"
    print inp_file 
    print"\n"
    print "+++++++++++++++++ Gadget header ++++++++++++++++++++++++++\n"
    
    snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
    snapshot_header =  snapshot.read_gadget_header()
    small_2D_real_cic(inp_file, Lbox, nbox, nfiles, npart_sim, snapID, dir_name)



if __name__ == "__main__":
    main()


