import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import sys
import matplotlib.gridspec as gridspec
import Cosmo_Growth_Fac_raw as CGF_raw
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.colors import ListedColormap
from astropy.cosmology import LambdaCDM
import readsnap
from lmfit.models import LinearModel

mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['font.sans-serif'] = ['Tahoma']
mpl.rcParams['text.usetex'] = True
plt.style.use("classic")


#------------ input Params ----------------------

# Remember nbox is ngrid not box size its Lbox

Lbox = np.int32(sys.argv[1])
nbox = np.int32(sys.argv[2])
snapnumber = np.int32(sys.argv[3])
npart_sim = np.int32(sys.argv[4])

#------------------------------------------------

#dir_name= ("/mnt/data2/nkhandai/new_runs/dmo/"\
#            "Gadget2/planck2018/%dMpc_400/"% (Lbox))

    
dir_name = "/mnt/data1/sandeep/New_Gadget2_run/%dMpc_%d/"% (Lbox, npart_sim)
inp_file = dir_name + "snapdir/snapshot_0%d.%d"%(snapnumber, 0)
print inp_file 
snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
print snapshot.read_gadget_header()
#    print "============Gadget header================"
print snapshot.read_gadget_header()

OmegaMatter = snapshot.Omega0
OmegaLambda = snapshot.OmegaLambda
H0 = snapshot.HubbleParam*100.

model = CGF_raw.Growth_Factor(h=snapshot.HubbleParam, Om=OmegaMatter, 
                               Onu=OmegaLambda, amin=1e-5, dlna=0.0001)

#snapshots = np.array([117, 108, 105, 102, 98, 94, 92, 89, 86, 85, 76, 67,
#                      62, 58, 50, 42, 36, 31, 27, 23, 19])  
#snapshots_redshift = np.array([0.00, 0.25, 0.35, 0.45, 0.55, 0.70, 0.75, 0.85,
#                           0.95, 1.0, 1.5, 2.0, 2.5, 3.0, 4.0, 5.0, 
#                           6.0, 7.0, 8.0, 9.0, 10.0])
#snapshots = np.array([117, 108, 98, 94, 89, 86, 85, 67,
#                       58, 50, 42, 36, 31, 27, 23, 19])  


snapshots = np.array([15, 14, 13, 12, 11, 10, 9,
                       8, 7, 6, 5, 4, 3, 2, 1, 0])  

snapshots_redshift = np.array([0.00, 0.25, 0.55, 0.70, 0.85,
                               0.95, 1.0, 2.0, 3.0, 4.0, 5.0, 
                               6.0, 7.0, 8.0, 9.0, 10.0])

bias = 1.

#snapshots_redshift = (1./snapshot_scale)-1

#----------------------------------------------------------------------------------

dirc_name = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/"\
            "%dMpc_%d_analysis/power_spec_data/%dMpc_%d_PowREAL/"%(Lbox, npart_sim, Lbox, npart_sim)
fin = dirc_name+"3dpower_dm_%dMpc_%d_%d.txt"%(Lbox, nbox, snapnumber)
K2     = np.genfromtxt(fin, usecols=0)



Delta2 = np.zeros((len(snapshots), len(K2)), dtype=np.float64)
err = np.zeros(len(snapshots), dtype=np.float64)
err = np.genfromtxt(fin, usecols=2)
factor_0 = np.zeros((len(snapshots)), dtype=np.float64)
count=0
for num in snapshots:
    D = model.growth_factor_fnc(snapshots_redshift[count])    
    beta = model.growth_rate_fnc(snapshots_redshift[count])/bias
    factor_0[count] = 1+(2.*beta/3) +(1.*beta**2/5)
    fin = dirc_name+"3dpower_dm_%dMpc_%d_%d.txt"%(Lbox, nbox, num)
    Delta2[count,:] = np.genfromtxt(fin, usecols=1)/ D**2
    count+=1

#factor_2 = (4.*f/3) +(4.*f**2/7)
#factor_4 = 8*f**2/35.

#----------------------------------------------------------------------------------

#dirc_name = "/mnt/data1/sandeep/Simulation_data/power_spec_data/200Mpc_400_PowRSD/"
dirc_name = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/"\
            "%dMpc_%d_analysis/power_spec_data/%dMpc_%d_PowRSD/"%(Lbox, npart_sim, Lbox, npart_sim)

Delta2_RSD = np.zeros((len(snapshots), len(K2)), dtype=np.float64)
count=0
for num in snapshots:
    D = model.growth_factor_fnc(snapshots_redshift[count])    
    fin = dirc_name+"3dpower_dm_%dMpc_%d_%d.txt"%(Lbox, nbox, num)
    Delta2_RSD[count,:] = np.genfromtxt(fin, usecols=1)/ D**2
    count+=1


#-----------------------------------------------------------
#dirc_name  = "/mnt/data2/nkhandai/new_runs/dmo/Gadget2/planck2018/200Mpc_400/"
#CAMB_K     = np.genfromtxt(dirc_name+"planck_2018_pofk_z0.dat", usecols=0)
#CAMB_Pk    = np.genfromtxt(dirc_name+"planck_2018_pofk_z0.dat", usecols=1)

dirc_name = "/mnt/data1/sandeep/New_Gadget2_run/%dMpc_%d/"%(Lbox, npart_sim)
CAMB_K     = np.genfromtxt(dirc_name+"multidark2_pofk_z0.dat", usecols=0)
CAMB_Pk    = np.genfromtxt(dirc_name+"multidark2_pofk_z0.dat", usecols=1)


norm_Pk = model.compute_sigma_sqr(CAMB_Pk, CAMB_K)


#----------------------------------------------------------------------------------



"""

fig= plt.figure(1, figsize=(15, 12))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])
NUM_COLORS = len(snapshots)
cm = plt.get_cmap('gnuplot2')
ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

for i in xrange(len(snapshots)):
    ax1.plot(np.log10(K2), np.log10(Delta2[i,:]),linestyle='-',
            linewidth=2.5, label=r"$\rm{P}^{%0.2f}_{m}(\rm{k})$"%snapshots_redshift[i])

ax1.plot((CAMB_K), np.log10(norm_Pk),linestyle='-', color='k',
         linewidth=2.5, label=r"$\rm{P}^{\rm{CAMB}}_{m}(\rm{k})$")

ax1.set_title(r"$\rm{Ngrid}:1024, REAL$", fontsize=16)
ax1.set_xlabel(r'$log_{10}(k)$', fontsize=20)
ax1.set_ylabel(r'$log_{10}(\Delta^2)$', fontsize=20)
#ax1.set_ylim(-1.5, 1.0)
#ax1.set_xlim(-1.7, 0.0)
ax1.set_ylim(-3, 3.2)
ax1.set_xlim(-2, 2)

ax1.minorticks_on()
ax1.legend(frameon=True, ncol=1, fontsize=12, loc='center left', bbox_to_anchor=(1, 0.5) )
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
#plt.tight_layout()
#plot_name = "/mnt/data1/sandeep/Simulation_data/plots/"
#fout = plot_name+"CompMatt_PowSpec_withCAMB_and_Diff_Snapshot_200Mpc_400_zoom_REAL.png"
plot_name = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/"\
            "%dMpc_%d_analysis/plots/"%(Lbox, npart_sim)
fout = plot_name+"CompMatt_PowSpec_withCAMB_and_Diff_Snapshot_%dMpc_%d_zoom_REAL.png"%(Lbox, npart_sim)
fig.savefig(fout, bbox_inches='tight')

#-------------------------------------------------------------------------------

fig= plt.figure(2, figsize=(15, 12))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])
NUM_COLORS = len(snapshots)
cm = plt.get_cmap('gnuplot2')
ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])

for i in xrange(len(snapshots)):
    ax1.plot(np.log10(K2), np.log10(Delta2_RSD[i,:]),linestyle='-',
            linewidth=2.5, label=r"$\rm{P}^{%0.2f}_{m}(\rm{k})$"%snapshots_redshift[i])

ax1.plot((CAMB_K), np.log10(norm_Pk),linestyle='-', color='k',
         linewidth=2.5, label=r"$\rm{P}^{\rm{CAMB}}_{m}(\rm{k})$")

ax1.set_title(r"$\rm{Ngrid}:1024, RSD$", fontsize=16)
ax1.set_xlabel(r'$log_{10}(k)$', fontsize=20)
ax1.set_ylabel(r'$log_{10}(\Delta^2)$', fontsize=20)
#ax1.set_ylim(-1.5, 1.0)
#ax1.set_xlim(-1.7, 0.0)

ax1.set_ylim(-3, 3.2)
ax1.set_xlim(-2, 2)


ax1.minorticks_on()
ax1.legend(frameon=True, ncol=1, fontsize=12, loc='center left', bbox_to_anchor=(1, 0.5) )
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
#plt.tight_layout()
#plot_name = "/mnt/data1/sandeep/Simulation_data/plots/"
plot_name = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/"\
            "%dMpc_%d_analysis/plots/"%(Lbox, npart_sim)
fout = plot_name+"CompMatt_PowSpec_withCAMB_and_Diff_Snapshot_%dMpc_%d_zoom_RSD.png"%(Lbox, npart_sim)
fig.savefig(fout, bbox_inches='tight')

#----------------------------------------------------------------
fig= plt.figure(3, figsize=(8, 6))
D = model.growth_factor_fnc(snapshots_redshift[0])    
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])
ax1.plot(np.log10(K2), np.log10(Delta2_RSD[0,:]),linestyle='-',
            linewidth=2.5, label=r"$\rm{P}^{real}_{m}$")
ax1.plot(np.log10(K2), np.log10(Delta2[0,:]),linestyle='-',
            linewidth=2.5, label=r"$\rm{P}^{real}_{m}$")
ax1.plot((CAMB_K), np.log10(norm_Pk),linestyle='-', color='k',
         linewidth=2.5, label=r"$\rm{P}^{\rm{CAMB}}_{m}(\rm{k})$")

ax1.set_title(r"$\rm{Ngrid}:1024, RSD$", fontsize=16)
ax1.set_xlabel(r'$log_{10}(k)$', fontsize=20)
ax1.set_ylabel(r'$log_{10}(\Delta^2)$', fontsize=20)
ax1.set_ylim(-3, 3)
ax1.set_xlim(-2, 2)
ax1.minorticks_on()
ax1.legend(frameon=False, ncol=1, fontsize=12 )
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
plt.tight_layout()


#----------------------------------------------------------------

fig= plt.figure(4, figsize=(15, 12))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])
#ax1.axhline(y=power_ratio[0], linestyle='--', linewidth=2.5, color='r', 
#            label=r"fit")
#ax1.axhline(y=factor_0, linestyle='--', linewidth=2.5, color='g', 
#            label=r"analytical")
#ax1.errorbar(K2, Delta/Delta2, yerr=Delta2_err, fmt='o', ecolor='k', 
#             elinewidth=2, capsize=5, mec='blue', alpha=0.7,
#             ms=8, mfc='none', mew=1.5, label=r"")
NUM_COLORS = len(snapshots)
cm = plt.get_cmap('nipy_spectral')
cm = plt.get_cmap('gnuplot2')
ax1.set_color_cycle([cm(1.*i/NUM_COLORS) for i in range(NUM_COLORS)])


for i in xrange(len(snapshots)):
    ax1.plot(K2, Delta2_RSD[i,:]/Delta2[i,:],linestyle='-',
            linewidth=2.5, label=r"$z=%0.2f$"%snapshots_redshift[i])

ax1.set_xscale("log")
ax1.set_yscale("log")
ax1.set_xlabel(r'$\rm{k}(\rm{h}/\rm{Mpc})$', fontsize=22)
ax1.set_ylabel(r'$\Delta^2_{\rm{s}}/\Delta^2_{\rm{r}}$', fontsize=22)
ax1.set_ylim(10**-1.5, 10**0.6)
ax1.set_xlim(0.02, 20)
ax1.minorticks_on()
ax1.legend(frameon=True, ncol=1, fontsize=12, loc='center left', bbox_to_anchor=(1, 0.5) )
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
plt.tight_layout()
#plot_name = "/mnt/data1/sandeep/Simulation_data/plots/"
plot_name = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/"\
            "%dMpc_%d_analysis/plots/"%(Lbox, npart_sim)
fout = plot_name+"PowSpec_Ratio_Diff_Snapshot_%dMpc_%d.png"%(Lbox, npart_sim)
fig.savefig(fout, bbox_inches='tight')
"""
"""
count = 0
fig= plt.figure(1, figsize=(22, 18))
gs = gridspec.GridSpec(4, 4, hspace=0.4, wspace=0.4)

for i in xrange(0, 4):
    for j in xrange(0, 4):
        
        ax1 = plt.subplot(gs[i, j])
        ax1.errorbar(K2, Delta2_RSD[count,:]/Delta2[count,:], yerr=err, fmt='o', ecolor='k', 
                     elinewidth=2.5, capsize=8, mec='blue',
                     ms=8, mfc='none', mew=2.0, label=r"")
        ax1.axhline(y= (Delta2_RSD[count,:]/Delta2[count,:])[0], linestyle='--', linewidth=2.5, 
                    color='r', label=r"$\rm{f}_{\rm{fit}}$:%0.4f"%(Delta2_RSD[count,:]/Delta2[count,:])[0])
                    
        ax1.axhline(y=factor_0[count], linestyle='--', linewidth=2.5, color='g', 
                    label=r"$\rm{f}_{\rm{theory}}$:%0.4f"%factor_0[count])

        ax1.set_title(r"$z=%0.2f$"%snapshots_redshift[count], fontsize=16)
        ax1.set_xscale("log")
        ax1.set_yscale("log")
        ax1.set_xlabel(r'$\rm{k}(\rm{h}/\rm{Mpc})$', fontsize=22)
        ax1.set_ylabel(r'$\Delta^2_{\rm{s}}/\Delta^2_{\rm{r}}$', fontsize=22)

        ax1.set_ylim(10**-1.5, 10**0.6)
#        ax1.set_xlim(, 0.2)
        ax1.minorticks_on()
        ax1.legend(frameon=False, loc=3, ncol=1, fontsize=20 )
        ax1.tick_params(axis='both', which='minor', length=5,
                              width=2, labelsize=16)
        ax1.tick_params(axis='both', which='major', length=8,
                               width=2, labelsize=16)
        ax1.spines['bottom'].set_linewidth(1.8)
        ax1.spines['left'].set_linewidth(1.8)
        ax1.spines['top'].set_linewidth(1.8)
        ax1.spines['right'].set_linewidth(1.8)
        count+=1

plt.tight_layout()
#plot_name = "/mnt/data1/sandeep/Simulation_data/plots/"
plot_name = "/mnt/data1/sandeep/New_Gadget2_run/post_simulation_analysis/"\
            "%dMpc_%d_analysis/plots/"%(Lbox, npart_sim)
fout = plot_name+"PowSpec_Ratio_Diff_Snapshot_%dMpc_%d_fit.png"%(Lbox, npart_sim)
fig.savefig(fout, bbox_inches='tight')
"""

plt.show()


