#************************************************************
"""
This function interpolates an irregularly sampled field to a  *
regular grid using Triangular Shaped Cloud (nearest grid point*
gets weight 0.75-dx**2, points before and after nearest grid  *
points get weight 0.5*(1.5-dx)**2, where dx is the distance   *
from the sample to the grid point in units of the cell size). *
"""
#**************************************************************







import numpy as np
import readsnap
import os
import sys
import pandas as pd
import itertools
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib as mpl 
import time
from matplotlib.colors import LogNorm
from matplotlib.colors import ListedColormap
from matplotlib.colors import LinearSegmentedColormap
from astropy.cosmology import LambdaCDM
from astropy import constants as const
import astropy.units as u
from numba import njit, jit
import argparse

mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['font.sans-serif'] = ['Tahoma']
mpl.rcParams['text.usetex'] = True
plt.style.use("classic")


@njit
def CIC_component(Nbox, NpartTotal, masspart, Pos1):
    """
    """
    
    
    denpart = np.zeros((Nbox, Nbox, Nbox), dtype=np.float64)
    
    TSC_W_x = np.zeros(3 , dtype=np.float64)
    TSC_W_y = np.zeros(3 , dtype=np.float64)
    TSC_W_z = np.zeros(3 , dtype=np.float64)
    
    #Compute CIC weights

    index_x = np.zeros(3 , dtype=np.int32)
    index_y = np.zeros(3 , dtype=np.int32)
    index_z = np.zeros(3 , dtype=np.int32)


        
    for i in xrange(NpartTotal):
        
        for j in xrange(-1, 2):
            
            intpos = np.rint(Pos1[i, 0])
            diff = np.fabs(intpos+j - Pos1[i, 0])
            index_x[j+1] = (intpos + j)%Nbox 

            if   j==-1: 
                 TSC_W_x[j+1] =  0.5 * (1.5-diff) * (1.5-diff)
            elif j==0: 
                 TSC_W_x[j+1] =  0.75 - diff * diff
            else:  
                 TSC_W_x[j+1] =  0.5 * (1.5-diff) * (1.5-diff)
            
            intpos = np.rint(Pos1[i, 1])
            diff = np.fabs(intpos+j - Pos1[i, 1])
            index_y[j+1] = (intpos + j)%Nbox 

           
            if   j==-1: 
                 TSC_W_y[j+1] =  0.5 * (1.5-diff) * (1.5-diff)
            elif j==0: 
                 TSC_W_y[j+1] =  0.75 - diff * diff
            else:  
                 TSC_W_y[j+1] =  0.5 * (1.5-diff) * (1.5-diff)


            intpos = np.rint(Pos1[i, 2])+0.5
            diff = np.fabs(intpos+j - Pos1[i, 2])
            index_z[j+1] = (intpos + j)%Nbox 

            if   j==-1: 
                 TSC_W_z[j+1] =  0.5 * (1.5-diff) * (1.5-diff)
            elif j==0: 
                 TSC_W_z[j+1] =  0.75 - diff*diff
            else:  
                 TSC_W_z[j+1] =  0.5 * (1.5-diff) * (1.5-diff)


        for l in xrange(3):  
            for m in xrange(3):  
                for n in xrange(3):
                    denpart[index_x[l], index_y[m], index_z[n]] += np.float64(masspart) * TSC_W_x[l] * TSC_W_y[m]* TSC_W_z[n]
       
        
    return denpart 


#------------ input Params ----------------------

def do_tsc_interp(dir_name, Lbox, nbox, nfiles, snapnumber):

    for i in xrange(nfiles):

        inp_file = dir_name + "snapdir/snapshot_%0.3d.%d"%(snapnumber, i)
        print inp_file 
        snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)

        if i==0:
            Positions_df  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
            Positions_df.columns=['x','y','z']

        if i==1:
            Positions_df1  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
            Positions_df1.columns=['x','y','z']

        if i==2:
            Positions_df2  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
            Positions_df2.columns=['x','y','z']

        if i==3:
            Positions_df3  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
            Positions_df3.columns=['x','y','z']


# -------------------------Merging all data frames---------------

    tmp = Positions_df.append(Positions_df1, ignore_index=True, sort=False)
    tmp1 = tmp.append(Positions_df2, ignore_index=True, sort=False)
    tmp2 = tmp1.append(Positions_df3, ignore_index=True, sort=False)
    Positions_df = tmp2

    del Positions_df1
    del Positions_df2 
    del Positions_df3

    print snapshot.HubbleParam
    npartTotal = snapshot.npartTotal[1]
        
    boxfac = np.float32(nbox/snapshot.BoxSize)
    #compute total masses
    totmdm = npartTotal*snapshot.massarr[1] #*1e10
    
    ParticleMass =snapshot.massarr[1]
    print "ParticleMass:", ParticleMass
    
    #rhodm_avg = (slice_width*boxfac)*totmdm/np.float64(self.nbox)**2.
    rhodm_avg = totmdm/np.float64(nbox)**3.
    print"-------------------------------------------"
    print "totodm : %f  rhodm_avg : %f"%(totmdm, rhodm_avg)
    print "The Total Number of Particles: %d"%npartTotal
    print"-------------------------------------------"
    
    Pos = Positions_df.to_numpy()
    Pos[:, 0] *= boxfac
    Pos[:, 1] *= boxfac
    Pos[:, 2] *= boxfac

    nbox = np.int64(nbox)
    npartTotal = np.int64(npartTotal)
    rhodm= CIC_component(nbox, npartTotal, ParticleMass, Pos)   

    print "rhodm_avg:", rhodm.sum()/np.float64(nbox)**3.
    rhodm = rhodm/rhodm_avg 
    print np.amax(rhodm)
    print np.amin(rhodm)
    

    #dir_name = "/mnt/data1/sandeep/Simulation_data/"
    #outfile=dir_name+"rhodm_%dMpc_%d_%d_tsc.npy"%(Lbox, np.int32(nbox), snapnumber)
    #np.save(outfile, rhodm)
    
 #    Sim_pow = Gen_Power_Sim.Sim_Power(np.int32(nbox), Lbox, snapnumber, npart_sim)
 #   Sim_pow.setk()
 #   Sim_pow.setweights()
 #   Sim_pow.setdeconv(False)
 #   Sim_pow.compute_3dpower(rhodm, 28, True)




def main():
    
    parser = argparse.ArgumentParser()
    parser.add_argument("Lbox", type=int, 
    help='BoxSize of the Simulation e.g 200Mpc')
    parser.add_argument("nbox", type=float, 
    help='Grid size for e.g 400, 1024 or 2048')
    parser.add_argument("snapID", type=int, 
    help='Snapshot ID e.g 117, 110, 005')
    parser.add_argument("nfiles", type=int, 
    help='Number of subfiles written by Gadget e.g snapshot.{0-4}')
    parser.add_argument("npart_sim", type=int, 
    help='Npart^3 used in simulation e.g npart_sim =256 for 256^3 particle simulation')
    
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()
    args = parser.parse_args()

#------------------------------------------------
    Lbox = args.Lbox
    nbox = args.nbox
    snapID = args.snapID
    nfiles = args.nfiles
    npart_sim = args.npart_sim
#------------------------------------------------

    dir_name= ("/mnt/data2/nkhandai/new_runs/dmo/Gadget2/planck2018/"\
                "%dMpc_%d/"% (Lbox, npart_sim))
    if nfiles<=1:
        inp_file = dir_name + "snapdir/snapshot_%03d"%(snapID)
    else:
        inp_file = dir_name + "snapdir/snapshot_%03d.%d"%(snapID, 0)

    print"\n"
    print inp_file 
    print"\n"
    print "+++++++++++++++++ Gadget header ++++++++++++++++++++++++++\n"
    
    do_tsc_interp(dir_name, Lbox, nbox, nfiles, snapID)


if __name__ == "__main__":
    main()


