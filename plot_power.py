import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import os
import sys
import matplotlib.gridspec as gridspec
import Cosmo_Growth_Fac_raw as CGF_raw
from matplotlib.colors import LogNorm
from matplotlib.colors import LinearSegmentedColormap
from matplotlib.colors import ListedColormap
from scipy.special import legendre
import readsnap
from astropy.cosmology import LambdaCDM
from lmfit.models import LinearModel


mpl.rcParams['font.family'] = 'sans-serif'
mpl.rcParams['font.sans-serif'] = ['Tahoma']
mpl.rcParams['text.usetex'] = True
plt.style.use("classic")


#------------ input Params ----------------------

# Remember nbox is ngrid not box size its Lbox

Lbox = np.int32(sys.argv[1])
nbox = np.float32(sys.argv[2])
snapnumber = np.int32(sys.argv[3])

#------------------------------------------------

dir_name= ("/mnt/data2/nkhandai/new_runs/dmo/"\
            "Gadget2/planck2018/%dMpc_400/"% (Lbox))

    
inp_file = dir_name + "snapdir/snapshot_%d.%d"%(snapnumber, 0)
print inp_file 
snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
#    print "============Gadget header================"
print snapshot.read_gadget_header()

OmegaMatter = snapshot.Omega0
OmegaLambda = snapshot.OmegaLambda
H0 = snapshot.HubbleParam*100.

Omega0 = OmegaMatter+OmegaLambda

def Hz(zepoch):
    return H0 * np.sqrt(OmegaMatter*(1+zepoch)**3+
                        (1-Omega0)*(1+zepoch)**2 +
                         OmegaLambda)
def Omega_z(y):
    return  OmegaMatter * (1.+y)**(3)  * H0**2 / Hz(y)**2
    



#---------------------------------------------------------------------



dirc_name = "/mnt/data1/sandeep/Simulation_data/power_spec_data/117_RSD/"


K0     = np.genfromtxt(dirc_name+"3dpower_dm_200Mpc_200_117.txt", usecols=0)
Delta0 = np.genfromtxt(dirc_name+"3dpower_dm_200Mpc_200_117.txt", usecols=1)
Kk     = np.genfromtxt(dirc_name+"3dpower_dm_200Mpc_100_117.txt", usecols=0)
Deltak = np.genfromtxt(dirc_name+"3dpower_dm_200Mpc_100_117.txt", usecols=1)
K1     = np.genfromtxt(dirc_name+"3dpower_dm_200Mpc_400_117.txt", usecols=0)
Delta1 = np.genfromtxt(dirc_name+"3dpower_dm_200Mpc_400_117.txt", usecols=1)
K2     = np.genfromtxt(dirc_name+"3dpower_dm_200Mpc_800_117.txt", usecols=0)
Delta2 = np.genfromtxt(dirc_name+"3dpower_dm_200Mpc_800_117.txt", usecols=1)
K3     = np.genfromtxt(dirc_name+"3dpower_dm_200Mpc_1024_117.txt", usecols=0)
Delta3 = np.genfromtxt(dirc_name+"3dpower_dm_200Mpc_1024_117.txt", usecols=1)

#K4     = np.genfromtxt(dirc_name+"3dpower_dm_200Mpc_2048.txt", usecols=0)
#Delta4 = np.genfromtxt(dirc_name+"3dpower_dm_200Mpc_2048.txt", usecols=1)



plot_name = "/mnt/data1/sandeep/Simulation_data/plots/"
fout = plot_name+"200Mpc_Comp_Box_Res_RSD.png"


fig= plt.figure(1, figsize=(8, 6))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])

ax1.plot(np.log10(Kk), np.log10(Deltak),linestyle='-',color='c'
        ,linewidth=2.5, label=r"$\rm{Ngrid}:100$")

ax1.plot(np.log10(K0), np.log10(Delta0),linestyle='-',color='m'
        ,linewidth=2.5, label=r"$\rm{Ngrid}:200$")

ax1.plot(np.log10(K1), np.log10(Delta1),linestyle='-',color='#1b9e77'
        ,linewidth=2.5, label=r"$\rm{Ngrid}:400$")

ax1.plot(np.log10(K2), np.log10(Delta2),linestyle='-',color='peru',
         linewidth=2.5, label=r"$\rm{Ngrid}:800$")

ax1.plot(np.log10(K3), np.log10(Delta3),linestyle='--', color='salmon',
        linewidth=2.5, label=r"$\rm{Ngrid}:1024$")

#ax1.plot(np.log10(K4), np.log10(Delta4),linestyle='-', color='blue',
#        alpha=0.7, linewidth=2.5, label=r"$Ngrid:2048$")


#ax1.set_xscale("log")
#ax1.set_yscale("log")
ax1.set_xlabel(r'$log_{10}(\rm{k})$', fontsize=16)
ax1.set_ylabel(r'$log_{10}(\Delta^2)$', fontsize=16)
ax1.set_ylim(-1.6, 2)
ax1.set_xlim(-2, 2)
ax1.minorticks_on()
ax1.legend(loc=4, frameon=False, ncol=2, fontsize=14)
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
fig.savefig(fout, bbox_inches='tight')

#-----------------------------------------------------------
dirc_name  = "/mnt/data2/nkhandai/new_runs/dmo/Gadget2/planck2018/200Mpc_400/"
CAMB_K     = np.genfromtxt(dirc_name+"planck_2018_pofk_z0.dat", usecols=0)
CAMB_Pk    = np.genfromtxt(dirc_name+"planck_2018_pofk_z0.dat", usecols=1)


dirc_name = "/mnt/data1/sandeep/Simulation_data/power_spec_data/117_REAL/"
K2     = np.genfromtxt(dirc_name+"3dpower_dm_200Mpc_1024_117.txt", usecols=0)
Delta2 = np.genfromtxt(dirc_name+"3dpower_dm_200Mpc_1024_117.txt", usecols=1)
Delta2_err = np.genfromtxt(dirc_name+"3dpower_dm_200Mpc_1024_117.txt", usecols=2)




model = CGF_raw.Growth_Factor(h=snapshot.HubbleParam, Om=OmegaMatter, 
                               Onu=OmegaLambda, amin=1e-5, dlna=0.0001)
f = model.growth_rate_fnc(snapshot.redshift)
D = model.growth_factor_fnc(snapshot.redshift)
f1 = Omega_z(snapshot.redshift)
print "================"
print "Normalized Growth factor:", D

factor_0 = 1+(2.*f/3) +(1.*f**2/5)
factor_2 = (4.*f/3) +(4.*f**2/7)
factor_4 = 8*f**2/35.





D_plus = D**2
norm_Pk = model.compute_sigma_sqr(CAMB_Pk, CAMB_K)
#----------------------------------------------------------------------------------

fig= plt.figure(2, figsize=(8, 6))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])

ax1.plot(np.log10(K2), np.log10(Delta2/D_plus),linestyle='-',color='peru',
         linewidth=2.5, label=r"$\rm{Ngrid}:1024, REAL$")
ax1.plot(np.log10(K3), np.log10(Delta3/D_plus),linestyle='--', color='teal',
        linewidth=2.5, label=r"$\rm{Ngrid}:1024, RSD$")
        
#ax1.plot((CAMB_K), 3*CAMB_K+(CAMB_Pk)-np.log10(2*np.pi**2),linestyle='--', color='r', alpha=0.7,
#ax1.plot(np.log10(M_K), np.log10(M_Pk),linestyle='', mec='r', mew=1.8,mfc='none',
#        marker='D', ms=6, label=r"$\rm{Matter Powerspec}$")

ax1.plot((CAMB_K), np.log10(norm_Pk),linestyle='-', color='b', alpha=0.7,
        linewidth=2.5, label=r"$\rm{CAMB}$")

ax1.set_xlabel(r'$log_{10}(k)$', fontsize=16)
ax1.set_ylabel(r'$log_{10}(\Delta^2)$', fontsize=16)
#ax1.set_ylabel(r'$log_{10}(P(k))$', fontsize=16)
ax1.set_ylim(-3, 3.5)
ax1.set_xlim(-2, 2)
ax1.minorticks_on()
ax1.legend(loc=4, frameon=False, ncol=1, fontsize=14)
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
fout = plot_name+"200Mpc_Comp_Box_Res_1024_with_CAMB.png"
#fout = plot_name+"ComapareMatterPowSpec_withCAMB.png"
fig.savefig(fout, bbox_inches='tight')

#------------------------------------------------------------------------------------



fout = plot_name+"200Mpc_Comp_Box_Res_1024_PowerRatio_Zoom.png"
power_ratio = Delta3/Delta2

"""
ind = (np.log10(K2)< -1.2)
fit_k =np.log10(K2)[ind] 
print fit_k
fit_power_ratio = np.log10(power_ratio)[ind]

print fit_power_ratio

y = fit_power_ratio 
x=fit_k
mod = LinearModel()
pars = mod.guess(y, x=x)
out = mod.fit(y, pars, x=x)
print(out.fit_report())

b = out.params['slope'].value
a = out.params['intercept'].value
"""


fig= plt.figure(3, figsize=(8, 6))
gs = gridspec.GridSpec(1, 1)
ax1 = plt.subplot(gs[0, 0])
ax1.axhline(y=power_ratio[0], linestyle='--', linewidth=2.5, color='r', 
            label=r"fit")
ax1.axhline(y=factor_0, linestyle='--', linewidth=2.5, color='g', 
            label=r"analytical")
ax1.errorbar(K2, Delta3/Delta2, yerr=Delta2_err, fmt='o', ecolor='k', 
             elinewidth=2, capsize=5, mec='blue', alpha=0.7,
             ms=8, mfc='none', mew=1.5, label=r"")

ax1.set_xscale("log")
ax1.set_yscale("log")
ax1.set_xlabel(r'$\rm{k}$', fontsize=22)
ax1.set_ylabel(r'$\Delta^2_{\rm{s}}/\Delta^2_{\rm{r}}$', fontsize=22)
ax1.set_ylim(10**-1.5, 10**0.6)
ax1.set_xlim(0.02, 0.6)
ax1.minorticks_on()
ax1.legend(loc=3, frameon=False, ncol=1, fontsize=14)
ax1.tick_params(axis='both', which='minor', length=5,
                      width=2, labelsize=16)
ax1.tick_params(axis='both', which='major', length=8,
                       width=2, labelsize=16)
ax1.spines['bottom'].set_linewidth(1.8)
ax1.spines['left'].set_linewidth(1.8)
ax1.spines['top'].set_linewidth(1.8)
ax1.spines['right'].set_linewidth(1.8)
plt.tight_layout()
fig.savefig(fout, bbox_inches='tight')

"""

#-----------------------------------------------------------------------

Lbox = 200
nbox = 400
plot_dir ="/mnt/data1/sandeep/Simulation_data/plots/"
file_dir ="/mnt/data1/sandeep/Simulation_data/rhodm_data/117_REAL/"
fin ="test_rhodm_2D_x-z_%dMpc_%d.npy"%(Lbox, np.int32(nbox))

rhodm = np.load(file_dir+fin)
print rhodm.shape

ind = (rhodm==np.amin(rhodm))
value = np.amin(rhodm[rhodm!=np.amin(rhodm)])
rhodm[ind] = value

print np.amax(np.log10(rhodm))
print np.amin(np.log10(rhodm))

#norm = mpl.colors.Normalize(vmin=np.amin(np.log10(rhodm)), vmax=np.amax(np.log10(rhodm)))
#norm = mpl.colors.Normalize(vmin=-4, vmax=2)
Splotch_colorscale = ['Blues_r']#, 'Reds',  'cubehelix', 'gist_heat']

for nam in Splotch_colorscale:
    cmap1 =  nam
    fout ="rhodm_2D_x-z_%dMpc_%d_%s.png"%(Lbox, np.int32(nbox), cmap1)
    fout = plot_dir+fout

    fig = plt.figure(1, figsize=(15, 10)) 
    gs = gridspec.GridSpec(1, 1)
    ax1 = plt.subplot(gs[0, 0]) 

    im = ax1.imshow(np.log10(rhodm), origin= 'lower', cmap=plt.cm.get_cmap(cmap1), 
                extent=[0, np.int32(Lbox), 0,  
                np.int32(Lbox)], interpolation='none')
 #               norm=norm)
                #norm = LogNorm(vmin=(np.amin(rhodm)*1.1), vmax=(np.amax(rhodm)*0.9)))

    ax1.set_ylabel(r"$\mathrm{z}(\rm{kpc}$ ${h}^{-1})$", fontsize=20)
    ax1.set_xlabel(r"$\mathrm{x}(\rm{kpc}$ ${h}^{-1})$", fontsize=20)

    ax1.set_title("Real space, Lbox= 200Mpc,  nbox=400")
    ax1.minorticks_on()
    ax1.legend(loc=4, frameon=False, ncol=1, fontsize=14)
    ax1.tick_params(axis='both', which='minor', length=2,
                          width=2, labelsize=16)
    ax1.tick_params(axis='both', which='major', length=5,
                           width=2, labelsize=16)
    ax1.spines['bottom'].set_linewidth(1.8)
    ax1.spines['left'].set_linewidth(1.8)
    ax1.spines['top'].set_linewidth(1.8)
    ax1.spines['right'].set_linewidth(1.8)
    plt.tight_layout()
    cbar = plt.colorbar(im)
    cbar.set_label(r"$\rho/\bar{\rho}$",fontsize=20)
    plt.savefig(fout, bbox_inches='tight')
    print"DONE\n"
    plt.close()

"""

plt.show()



