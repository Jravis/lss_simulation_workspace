import numpy as np
from numba import njit, jit
from astropy.cosmology import LambdaCDM
from astropy import constants as const
import astropy.units as u
import pandas as pd
from multiprocessing import Process
import itertools
import sys
import os
import time
#----------My modules for Gadget2----------------
import readsnap
#------------ input Params ----------------------

# Remember nbox is ngrid not box size its Lbox

#        dir_name= ("/mnt/data2/nkhandai/new_runs/dmo/"\
#                   "Gadget2/planck2018/%dMpc_%d/"% (Lbox, npart_sim))

#---------------------------------------------------------


#--------------------------------------------------------------------------
#                             sign(x,y)
#--------------------------------------------------------------------------
#   This routine transfers the sign of y to x
#--------------------------------------------------------------------------
@jit
def sign(x, y): 

    if y >= 0:
        return(np.abs(x))
    else:
        return(-np.abs(x))
#sign()

@jit
def periodic_check(bhalf, lbox, dr):
        
    tmp=0.
    tmp = np.abs(dr)
    if tmp>=bhalf:
        sr=sign(1., dr)
        tmp = tmp-lbox
        dr = sr*tmp

    return dr


@jit
def nested_loop(sub_size, boxhalf, Lbox, Rmax, pos, vel):
   
    tmp=0.
    dr2 = []
    dv2 = []
    dr_dot_dv = []
    
    for j in xrange(100000):#sub_size):
        for i in xrange(j+1, 100000):#sub_size):
        
            dx = pos[i, 0]-pos[j, 0]
            dy = pos[i, 1]-pos[j, 1]
            dz = pos[i, 2]-pos[j, 2]
            dx = periodic_check(boxhalf, Lbox, dx)
            dy = periodic_check(boxhalf, Lbox, dy)
            dz = periodic_check(boxhalf, Lbox, dz)

            dvx = vel[i, 0]-vel[j, 0]
            dvy = vel[i, 1]-vel[j, 1]
            dvz = vel[i, 2]-vel[j, 2]
            
            drr2 = dx**2 + dy**2 + dz**2 
            d = np.sqrt(drr2)
            if 0 < d < Rmax:
                dr2.append(dx**2 + dy**2 + dz**2)
                dv2.append(dvx**2 + dvy**2 + dvz**2)
                dr_dot_dv.append(dvx*dx + dvy*dy + dvz*dz)

    return  np.asarray(dr2), np.asarray(dv2), np.asarray(dr_dot_dv)


#-------------------------------------------------------------

class Correlation:

#------- class to compute pair correaltion

    def __init__(self, dir_name, nfiles, npart_sim, analysis_type): 
        
        """
        Parameters:
        """
        
        dir_check = str(os.path.isdir(dir_name))
        if dir_check==False:
            raise ValueError('Input directory path does not exsist')
        

        self.dir_name = dir_name
        self.nfiles    = nfiles
        self.npart_sim = npart_sim
        
        
        for root, dirs, files in os.walk(self.dir_name, topdown=False):
            for name in files:
                if name.startswith("snapshot")==True:
                    #print(os.path.join(root,name))
                    print (os.path.join(name))

        print ""
        print("Enter the snapshot number you want to read\n")
        print ""
        self.snapnumber=np.int32(raw_input(""))
        
        #------------------------------------------------
        
        inp_file = self.dir_name + "snapshot_%0.3d.%d"%(self.snapnumber, 0)
        header = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)
        
        print "============Gadget header================\n"
        
        print header.read_gadget_header()
        
        print "============Gadget header================\n"
        

        self.header  = header
        self.ParticleMass = header.massarr[1]
        self.cosmo  = LambdaCDM(H0= 100.*header.HubbleParam, 
                               Om0 = header.Omega0,
                               Ode0= header.OmegaLambda,
                               Tcmb0=0.)
                       
        self.analysis_type = analysis_type
        if analysis_type =='LCDM':
            self.nbox    = header.BoxSize/1000.
            self.boxhalf = self.nbox/2
        else: 
            print("Enter the nbox\n")
            print ""
            self.nbox=np.int32(raw_input(""))
            self.boxhalf = self.nbox/2
            self.boxfac = np.float32(nbox/header.BoxSize)

    #---------------------------------------------------------------------

    def Sampling_routine(self, nsubs):

        """
        """


        for i in xrange(self.nfiles):
            inp_file = self.dir_name + "snapshot_%03d.%d"%(self.snapnumber, i)
            file_check = str(os.path.isfile(inp_file))
            if file_check==False:
                raise ValueError('Input file does not exsist')
            snapshot = readsnap.Gadget2_snapshot_hdr(inp_file, DataFrame=True)

            if i==0:
                Positions_df  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
                Positions_df.columns=['x','y','z']
                Velocity_df   = snapshot.read_Vel()
                Velocity_df.columns=['vx','vy','vz']

            if i==1:
                Positions_df1  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
                Positions_df1.columns=['x','y','z']
                Velocity_df1   = snapshot.read_Vel()
                Velocity_df1.columns=['vx','vy','vz']

            if i==2:
                Positions_df2  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
                Positions_df2.columns=['x','y','z']
                Velocity_df2   = snapshot.read_Vel()
                Velocity_df2.columns=['vx','vy','vz']

            if i==3:
                Positions_df3  = snapshot.read_Pos() # is u.kpc*snapshot.HubbleParam**(-1)
                Positions_df3.columns=['x','y','z']
                Velocity_df3   = snapshot.read_Vel()
                Velocity_df3.columns=['vx','vy','vz']

        #--------------- Merging all data frames ---------------

        tmp = Positions_df.append(Positions_df1, ignore_index=True, sort=False)
        tmp1 = tmp.append(Positions_df2, ignore_index=True, sort=False)
        tmp2 = tmp1.append(Positions_df3, ignore_index=True, sort=False)
        Positions_df = tmp2

        #--------------- Merging all Vel data frame ---------------

        tmp = Velocity_df.append(Velocity_df1, ignore_index=True, sort=False)
        tmp1 = tmp.append(Velocity_df2, ignore_index=True, sort=False)
        tmp2 = tmp1.append(Velocity_df3, ignore_index=True, sort=False)
        Velocity_df = tmp2
        
        #--------------- Delete all unused data frame -------------

        del Velocity_df1;  del Velocity_df2;  del Velocity_df3
        del Positions_df1; del Positions_df2; del Positions_df3

        print("Enter the path of directory you want to"\
              " create for subsamples ")
              
        PATH = raw_input("")
        dir_check = str(os.path.isdir(PATH))
        print dir_check 

        if dir_check=='False':
            os.mkdir(PATH)
            print("Directory is created\n")
            #raise ValueError('Oops something went wrong and directory is not created\n')
        else:
            print("Directory is already there\n")
        
        print("You can either give lenght of subsample"\
              "or fraction of total sample e.g 20% of"\
              "total sample\n")
              

        print ("Type Yes for frac or enter the length of"\
               "subsample you want\n")
        
        in_key = raw_input("")

        if in_key == 'Yes':
            print ("Enter the fraction of total sample you want to sample\n")
            fraction_sample = np.float64(raw_input(""))
        else:
            print("Enter the length of subsample you want\n")
            nsamples = np.int32(raw_input(""))
            
        
        self.seed = 134651451
        scale_fac = 1./(1.+self.header.redshift)
        kpc_to_Mpc = 1./1000.
        
        #To get comoving Peculiar velocities from Gadget 
        #velocities multiply by this factor
        
        vel_conv_factor = 1./np.sqrt(scale_fac)

        for i in xrange(nsubs):
            
            if in_key=='Yes':
                sub_sample_Pos = Positions_df.sample(frac=fraction_sample, 
                                                    random_state=self.seed, 
                                                    replace=False)
                sub_sample_Vel = Velocity_df.sample(frac=fraction_sample, 
                                                    random_state=self.seed, 
                                                    replace=False)
            else:
                sub_sample_Pos = Positions_df.sample(n=nsamples, 
                                                     random_state=self.seed, 
                                                     replace=False)
                sub_sample_Vel = Velocity_df.sample(n=nsamples, 
                                                    random_state=self.seed, 
                                                    replace=False)
            #----------------------------------------------------------------                                                    
            
            Pos   = sub_sample_Pos.to_numpy(dtype = np.float64)
            Vel   = sub_sample_Vel.to_numpy(dtype = np.float64)
            fname = (PATH+"/Pos_sample_%d_%d_%d_%d_%d.bin"%(np.int32(self.header.BoxSize/1000.), 
                                            self.npart_sim, self.snapnumber, i, self.seed))
            fname1 = (PATH+"/Vel_sample_%d_%d_%d_%d_%d.bin"%(np.int32(self.header.BoxSize/1000.), 
                                            self.npart_sim, self.snapnumber, i, self.seed))
            
            if self.analysis_type =='LCDM':
                Pos[:, 0] *= kpc_to_Mpc  
                Pos[:, 1] *= kpc_to_Mpc 
                Pos[:, 2] *= kpc_to_Mpc    
            else:
                Pos[:, 0] *= self.boxfac
                Pos[:, 1] *= self.boxfac
                Pos[:, 2] *= self.boxfac
                
            Pos.tofile(fname)
            
            Vel[:, 0] *= vel_conv_factor
            Vel[:, 1] *= vel_conv_factor
            Vel[:, 2] *= vel_conv_factor
            Vel.tofile(fname1)
            
        np.savetxt(PATH+"/sample_size.txt", zip([len(Pos[:, 0])], [nsubs]), fmt="%d")
        

    #--------------------------------------------------------------------------------------
    
    
    def pair_corr(self, nbin, rmin, rmax, bin_type='log'):
        """
        """
        
        print("Enter the path of subsamples directory\n")
              
        PATH = raw_input("")
        dir_check = str(os.path.isdir(PATH))
        if dir_check==False:
            raise ValueError('Oops something went wrong and directory is not created')
        
        samp_size = np.int32(np.genfromtxt(PATH+"/sample_size.txt", usecols=0))
        #nsubs = np.int32(np.genfromtxt(PATH+"/sample_size.txt", usecols=1))
        nsubs = 1

        rbin = np.logspace(np.log10(rmin), np.log10(rmax), nbin)
        vol = np.zeros(nbin, dtype=np.float64)
        
        vol[0]=(4.0*np.pi/3.0)*rbin[0]**3.0
        for i in xrange(1, nbin):
            vol[i]=(4.0*np.pi/3.0)*((rbin[i]**3.0) - (rbin[i-1]**3.0))
    
        numden=np.float64(samp_size)/np.float64(self.header.npartTotal[1]);
        avpart=0.5*np.float64(samp_size)*vol*numden
        avpart=vol
        print numden, np.float64(samp_size), self.header.npartTotal[1]

        self.nbin = nbin

        pairvel = np.zeros((nsubs, nbin), dtype=np.float64)
        pairvdisp = np.zeros((nsubs, nbin), dtype=np.float64)
        pairs = np.zeros((nsubs, nbin), dtype=np.float64)
        xi = np.zeros((nsubs, nbin), dtype=np.float64)
        
        dt = np.dtype((np.float64, 3))  # Positions are in float not double
        
#        for root, dirs, files in os.walk(PATH, topdown=False):
#          for name in files:
#            if name.endswith(".bin")==True:

#                if name.startswith("Pos")==True:
#                    print(os.path.join(root, name))
#                    print"\n"
#                    f = os.path.join(root, name)
#                    Pos = np.fromfile(f,dtype=dt,count=samp_size)
                    
#                if name.startswith("Vel")==True:
#                    print(os.path.join(root, name))
#                    print"\n"
#                    f = os.path.join(root, name)
#                    Vel = np.fromfile(f,dtype=dt,count=samp_size)
               
        for count in xrange(nsubs):
            
            f = PATH+"/Pos_sample_1000_256_5_%d_134651451.bin"%count
            Pos = np.fromfile(f,dtype=dt,count=samp_size)
            f = PATH+"/Vel_sample_1000_256_5_%d_134651451.bin"%count
            Vel = np.fromfile(f,dtype=dt,count=samp_size)
            dr2, dv2, dr_dot_dv = nested_loop(samp_size, self.boxhalf, self.header.BoxSize/1000., rmax, Pos, Vel)
            print("Nested loop is done\n")
            d = np.sqrt(dr2)
            #ind = (d < rmax)
            #d1 = d[ind]
#            dr2 = dr2[ind]
#            dv2 = dv2[ind]
#            dr_dot_dv = dr_dot_dv[ind]
            index = np.digitize(d, rbin)
            #index = np.searchsorted(rbin, d, side='left')
            print index
            print rbin
            for nn in index:
                pairs[count, nn] += 1
                pairvel[count, nn] += dr_dot_dv[nn]/d[nn]
                pairvdisp[count, nn] += dv2[nn]/dr2[nn]
            print pairs[count, :] 
            print avpart
            xi[count,:] = (pairs[count, :]/avpart)-1
            print xi[0,:]
            del Pos 
            del Vel
        #------------------------------------------------------------------
        print count, nsubs
        xi_bar = np.zeros((nsubs, nbin+1), dtype=np.float64)
        for i in xrange(count):
            for j in xrange(nbin):
                sum1 = sum1+avpart[j]
                sum2 = sum2+pairs[count, j]
                xi_bar[count, j] = sum2/sum1 - 1
            pairvel[count, :] = pairvel[count, :]/pairs[count, :]
            pairvdisp[count, :] = pairvdisp[count, :]/pairs[count, :]

        xi_av        = np.mean(xi, axis=0)
        xibar_av     = np.mean(xi_bar, axis=0)
        pairvel_av   = np.mean(pairvel, axis=0)
        pairvdisp_av = np.mean(pairvdisp, axis=0)

        xi_av_err        = np.std(xi, axis=0)
        xibar_av_err     = np.std(xi_bar, axis=0)
        pairvel_av_err   = np.std(pairvel, axis=0)
        pairvdisp_av_err = np.std(pairvdisp, axis=0)


        fname = PATH+"/corr_%dMpc_%d.txt"%(self.header.BoxSize/1000, self.snapnumber)
        np.savetxt(fname, zip(rbin, avpart, xi_av, xi_av_err, xibar_av, xibar_av_err), fmt="%2.14f", 
                header='rbin\tavpart\txi_av\txi_av_err\txibar_av\txibar_av_err', delimiter='\t')  
                
        fname = PATH+"/pairv_%dMpc_%d.txt"%(self.header.BoxSize/1000, self.snapnumber)
        np.savetxt(fname, zip(rbin, pairvel_av, pairvel_av_err, pairvdisp_av, pairvdisp_av_err), fmt="%2.14f", 
                header='rbin\tpairvel_av\tpairvel_av_err\tpairvdisp_av\tpairvdisp_av_err', delimiter='\t')  




