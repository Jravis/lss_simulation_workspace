import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import matplotlib as mpl 
from matplotlib.colors import ListedColormap
from matplotlib.colors import LinearSegmentedColormap
import matplotlib.patches as patches


def whiteblue():
    from matplotlib.colors import ListedColormap

#whiteblue
    c = np.asarray([[1, 1, 1],
                    [0.7, 0.7, 0.7],
                    [0, 0.2, 0.4],
                    [0, 0, 0.2],
                    [0.9, 0.17, 0],
                    [1, 0.8, 0],
                    [1, 1, 0.3]])
    pt = np.asarray([0, 0.1, 0.4, 0.5, 0.65, 0.8, 1.0])
    c_r, c_g, c_b = c.T
    print c_r
    print c_g
    print c_b
    out_pt = np.linspace(0, 1.0, 255)
    out_r = np.interp(out_pt, pt, c_r)
    out_g = np.interp(out_pt, pt, c_g)
    out_b = np.interp(out_pt, pt, c_b)
    colormap_data = np.vstack([out_r, out_g, out_b]).T
    return ListedColormap(colormap_data)


def my_green():
    from matplotlib.colors import ListedColormap
#green
    c = np.asarray([[1, 1, 1],
                    [0.8, 0.8, 0.8],
                    [0.02, 0.02, 0.02],
                    [0, 0, 0.2],
                    [0.9, 0.17, 0],
                    [1, 0.8, 0.1],
                    [0.8, 0.8, 0.]])
                    
    pt = np.asarray([0, 0.1, 0.4, 0.5, 0.65, 0.8, 1.0])
    c_r, c_g, c_b = c.T
    print c_r
    print c_g
    print c_b
    out_pt = np.linspace(0, 1.0, 255)
    out_r = np.interp(out_pt, pt, c_r)
    out_g = np.interp(out_pt, pt, c_g)
    out_b = np.interp(out_pt, pt, c_b)
    colormap_data = np.vstack([out_r, out_g, out_b]).T
    return ListedColormap(colormap_data)


def my_black():
    from matplotlib.colors import ListedColormap

#black
    c = np.asarray([[1, 1, 1],
                    [0.9, 0.9, 0.9],
                    [0.01, 0.01, 0.01],
                    [0, 0, 0],
                    [0.9, 0.17, 0],
                    [1, 0.8, 0.1],
                    [0.8, 0.8, 0.]])

    pt = np.asarray([0, 0.1, 0.4, 0.5, 0.65, 0.8, 1.0])
    c_r, c_g, c_b = c.T
    print c_r
    print c_g
    print c_b
    out_pt = np.linspace(0, 1.0, 255)
    out_r = np.interp(out_pt, pt, c_r)
    out_g = np.interp(out_pt, pt, c_g)
    out_b = np.interp(out_pt, pt, c_b)
    colormap_data = np.vstack([out_r, out_g, out_b]).T
    return ListedColormap(colormap_data)



def my_blue():
    from matplotlib.colors import ListedColormap

#blue
    c = np.asarray([[1, 1, 1],
                    [0.8, 0.8, 0.8],
                    [0.02, 0.02, 0.02],
                    [0, 0.2, 0],
                    [0.9, 0.17, 0],
                    [1, 0.8, 0.1],
                    [0.9, 0.9, 0.]])

    pt = np.asarray([0, 0.1, 0.4, 0.5, 0.65, 0.8, 1.0])
    c_r, c_g, c_b = c.T
    print c_r
    print c_g
    print c_b
    out_pt = np.linspace(0, 1.0, 255)
    out_r = np.interp(out_pt, pt, c_r)
    out_g = np.interp(out_pt, pt, c_g)
    out_b = np.interp(out_pt, pt, c_b)
    colormap_data = np.vstack([out_r, out_g, out_b]).T
    return ListedColormap(colormap_data)






